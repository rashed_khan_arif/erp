<?php

class Admin_model extends CI_Model {

    public function check_admin_info($email_address, $password) {
        $this->db->select('*');
        $this->db->from('tbl_admin');
        $this->db->where('admin_email', $email_address);
        $this->db->where('password', $password);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    public function save_employee_information($data) {
        $this->db->insert('tbl_employee', $data);
    }

    public function view_employee_info() {
        $this->db->select('*');
        $this->db->from('tbl_employee');
        $this->db->where('status', 1);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function view_inactive_employee_info() {
        $this->db->select('*');
        $this->db->from('tbl_employee');
        $this->db->where('status', 0);
        $this->db->order_by('emp_id', 'DESC');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function view_en_employee_info($em_id) {
        $this->db->select('*');
        $this->db->from('tbl_employee');
        $this->db->where('emp_id', $em_id);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    public function view_employee_info_by_value($search_text) {
        if ($search_text == NULL) {
            $this->db->select('*');
            $this->db->from('tbl_employee');
            $query = $this->db->get();
            $result = $query->result();
            return $result;
        } else {
            $sql = "SELECT * FROM tbl_employee WHERE employee_id LIKE '%$search_text%' or first_name LIKE '%$search_text%' or last_name LIKE '%$search_text%'";
            $query = $this->db->query($sql);
            $result = $query->result();
            return $result;
        }
    }

    public function view_item_info_by_value($search_text) {
        if ($search_text == NULL) {
            $this->db->select('*');
            $this->db->from('tbl_item');
            $query = $this->db->get();
            $result = $query->result();
            return $result;
        } else {
            $sql = "SELECT * FROM tbl_item WHERE item_id LIKE '%$search_text%'";
            $query = $this->db->query($sql);
            $result = $query->result();
            return $result;
        }
    }

    public function view_custo_info_by_value($search_text) {
        if ($search_text == NULL) {
            $this->db->select('*');
            $this->db->from('tbl_customer');
            $query = $this->db->get();
            $result = $query->result();
            return $result;
        } else {
            $sql = "SELECT * FROM tbl_customer WHERE customer_id LIKE '%$search_text%' or organization_name Like '%$search_text%'";
            $query = $this->db->query($sql);
            $result = $query->result();
            return $result;
        }
    }

    public function active_employee_info($emp_id) {
        $this->db->set('status', 1);
        $this->db->where('emp_id', $emp_id);
        $this->db->update('tbl_employee');
    }

    public function published_inactive_info($emp_id) {
        $this->db->set('status', 1);
        $this->db->where('emp_id', $emp_id);
        $this->db->update('tbl_employee');
    }

    public function un_published_employee_info($emp_id) {
        $this->db->set('status', 0);
        $this->db->where('emp_id', $emp_id);
        $this->db->update('tbl_employee');
    }

    public function un_published_inactive_info($emp_id) {
        $this->db->set('status', 0);
        $this->db->where('emp_id', $emp_id);
        $this->db->update('tbl_employee');
    }

    public function select_pre_image($emp_id) {
        $this->db->select('*');
        $this->db->from('tbl_employee');
        $this->db->where('emp_id', $emp_id);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    public function update_employee_information($emp_id, $data) {
        $this->db->where('emp_id', $emp_id);
        $this->db->update('tbl_employee', $data);
    }

    public function save_salary_information($data) {
        $this->db->insert('tbl_salary', $data);
        $id = $this->db->insert_id();
        return $id;
    }

//    public function update_salary_information($data) {
//        $this->db->where('tbl_salary', $data);
//        $id = $this->db->insert_id();
//        return $id;
//    }

    public function save_salary_monyear($sddata) {
        $this->db->insert('tbl_salary_my', $sddata);
    }

    public function update_items($item_no, $data) {
        $this->db->where('item_no', $item_no);
        $this->db->update('tbl_item', $data);
    }

    public function update_salary_information($id, $data) {
        $this->db->where('si_id', $id);
        $this->db->update('tbl_salary', $data);
    }

    public function delete_salary_info($id) {
        $this->db->where('si_id', $id);
        $this->db->update('tbl_salary');
    }

    public function save_items($data) {
        $this->db->insert('tbl_item', $data);
    }

    public function select_all_item() {
        $this->db->select('*');
        $this->db->from('tbl_item');
        $this->db->order_by('item_no', 'DESC');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function select_all_item_by_id($id) {
        $this->db->select('*');
        $this->db->from('tbl_item');
        $this->db->where('item_no', $id);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    public function delete_items($id) {
        $this->db->where('item_no', $id);
        $this->db->delete('tbl_item');
    }

    public function delete_customers($id) {
        $this->db->where('c_id', $id);
        $this->db->delete('tbl_customer');
    }

    public function save_customer_info($data) {
        $this->db->insert('tbl_customer', $data);
    }

    public function select_all_customer() {
        $this->db->select('*');
        $this->db->from('tbl_customer');
        $this->db->order_by('c_id', 'DESC');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function select_all_customer_id($id) {
        $this->db->select('*');
        $this->db->from('tbl_customer');
        $this->db->where('c_id', $id);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    public function update_customer_info($c_id, $data) {
        $this->db->where('c_id', $c_id);
        $this->db->update('tbl_customer', $data);
    }

    public function save_product_info($data) {
        $this->db->insert('tbl_product', $data);
    }

    public function select_all_product() {
        $this->db->select('*');
        $this->db->from('tbl_product');
        $this->db->order_by('p_id', 'DESC');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function select_all_product_id($id) {
        $this->db->select('*');
        $this->db->from('tbl_product');
        $this->db->where('p_id', $id);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    public function update_product_info($p_id, $data) {
        $this->db->where('p_id', $p_id);
        $this->db->update('tbl_product', $data);
    }

    public function view_product_info_by_value($search_text) {
        if ($search_text == NULL) {
            $this->db->select('*');
            $this->db->from('tbl_product');
            $this->db->order_by('p_id', 'DESC');
            $query = $this->db->get();
            $result = $query->result();
            return $result;
        } else {
            $sql = "SELECT * FROM tbl_product WHERE product_id LIKE '%$search_text%' or product_name Like '%$search_text%'";
            $query = $this->db->query($sql);
            $result = $query->result();
            return $result;
        }
    }

    public function view_product_info_by_value_id($search_text) {
        if ($search_text == NULL) {
            $this->db->select('*');
            $this->db->from('tbl_product');
            $this->db->order_by('p_id', 'DESC');
            $query = $this->db->get();
            $result = $query->result();
            return $result;
        } else {
            $sql = "SELECT * FROM tbl_product WHERE product_id LIKE '%$search_text%'";
            $query = $this->db->query($sql);
            $result = $query->result();
            return $result;
        }
    }

    public function view_invoice_info_by_value_id($search_text) {
        if ($search_text == NULL) {
            echo "";
        } else {
            $sql = "SELECT c.*,o.*,p.* From tbl_customer as c,tbl_order as o,tbl_payment as p Where c.c_id=o.c_id AND o.payment_id=p.payment_id AND o.order_id LIKE '%$search_text%' order by o.order_id DESC";
            $query = $this->db->query($sql);
            $result = $query->result();
            return $result;
        }
    }

    public function select_all_order() {
        $sql = "SELECT c.*,o.*,p.* From tbl_customer as c,tbl_order as o,tbl_payment as p Where c.c_id=o.c_id AND o.payment_id=p.payment_id order by o.order_id DESC";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    public function select_all_unpaid_order() {
        $sql = "SELECT c.*,o.*,p.* From tbl_customer as c,tbl_order as o,tbl_payment as p Where c.c_id=o.c_id AND o.payment_id=p.payment_id AND p.cash < o.order_total  AND p.payment_type !='bank' order by o.order_id DESC";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    public function select_order_info($o_id) {
        $sql = "SELECT c.*,o.*,p.*,e.* From tbl_customer as c,tbl_order as o,tbl_payment as p,tbl_employee as e Where c.c_id=o.c_id AND o.payment_id=p.payment_id AND p.emp_id=e.emp_id AND o.order_id=$o_id";
        $query = $this->db->query($sql);
        $result = $query->row();
        return $result;
    }

    public function select_unpaid_order_info($id) {
        $sql = "SELECT o.*,p.* From tbl_order as o,tbl_payment as p Where o.payment_id=p.payment_id AND o.order_id=$id";
        $query = $this->db->query($sql);
        $result = $query->row();
        return $result;
    }

    public function update_order_payment($payment_id, $cash) {
        $sql = "UPDATE tbl_payment SET cash=cash+$cash Where payment_id=$payment_id";
        $this->db->query($sql);
    }

    public function select_product_info($id) {
        $sql = "SELECT d.*,p.* From tbl_order_details as d,tbl_product as p Where d.p_id=p.p_id AND d.order_id=$id";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    public function check_amount($payment_id) {
        $sql = "SELECT o.*,p.* From tbl_order as o,tbl_payment as p Where o.payment_id=p.payment_id AND o.payment_id=$payment_id";
        $query = $this->db->query($sql);
        $result = $query->row();
        return $result;
//              $this->db->select('*');
//        $this->db->from('tbl_payment');
//        $this->db->where('payment_id', $payment_id);
//        $query = $this->db->get();
//        $result = $query->row();
//        return $result;
    }

    public function view_employee_info_by_id($id) {
        $this->db->select('*');
        $this->db->from('tbl_employee');
        $this->db->where('emp_id', $id);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    public function view_employee_salary_info() {
        $sql = "SELECT e.*,s.* from tbl_employee as e, tbl_salary as s Where e.emp_id=s.emp_id AND e.status=1 group by s.emp_id order by s.si_id DESC";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    public function select_all_salary_by_id($id) {
        $sql = "SELECT e.*,s.* from tbl_employee as e, tbl_salary as s Where e.emp_id=s.emp_id AND s.si_id=$id";
        $query = $this->db->query($sql);
        $result = $query->row();
        return $result;
    }

    public function view_single_employee_salary_info($id) {
        $sql = "SELECT e.*,s.* from tbl_employee as e, tbl_salary as s Where e.emp_id=s.emp_id AND e.emp_id=$id";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    public function view_single_employee_pay_salary_info($emp_id) {
        $this->db->select('*');
        $this->db->from('tbl_pay_salary');
        $this->db->where('emp_id', $emp_id);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function check_employee($month, $year, $emp_id) {
        $this->db->select('*');
        $this->db->from('tbl_salary');
        $this->db->where('month', $month);
        $this->db->where('year', $year);
        $this->db->where('emp_id', $emp_id);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    public function view_unpaid_employee_info() {
        // $sql = "SELECT s.*,p.si_id,e.* From tbl_salary as s, tbl_pay_salary as p,tbl_employee as e Where s.emp_id=e.emp_id AND s.si_id=p.si_id AND e.status=1";
        $sql = "SELECT e.*,s.* FROM tbl_salary as s,tbl_employee as e WHERE e.emp_id=s.emp_id AND e.status=1 AND s.si_id NOT IN(SELECT si_id FROM tbl_pay_salary)";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    public function select_employee_info($si_id, $emp_id) {
        $sql = "SELECT e.*,s.* from tbl_employee as e, tbl_salary as s Where s.si_id=$si_id AND e.emp_id=$emp_id";
        $query = $this->db->query($sql);
        $result = $query->row();
        return $result;
    }

    public function select_employee_month_year_info($emp_id) {
        $sql = "SELECT * from tbl_salary Where si_id NOT IN(SELECT si_id FROM tbl_pay_salary) AND emp_id=$emp_id ";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    public function save_salary_payment_info($data) {
        $this->db->insert('tbl_pay_salary', $data);
    }

}
