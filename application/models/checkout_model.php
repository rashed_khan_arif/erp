<?php

/**
 * Description of checkout_model
 *
 * @author Rashed khan
 */
class checkout_model extends CI_Model {

    public function select_cust($customer_id) {
        $this->db->select('c_id');
        $this->db->from('tbl_customer');
        $this->db->where('customer_id', $customer_id);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    public function save_payment_info($pdata) {
        $this->db->insert('tbl_payment', $pdata);
        $payment_id = $this->db->insert_id();
        $data = array();
        $data['payment_id'] = $payment_id;
        $this->session->set_userdata($data);
    }

    public function save_order_info() {
        $data = array();
        $data['payment_id'] = $this->session->userdata('payment_id');
        $data['c_id'] = $this->session->userdata('c_id');
        $data['discount'] = $this->session->userdata('dis');
        $data['vat'] = $this->session->userdata('vat');
        $data['order_total'] = $this->session->userdata('g_t');
        $this->db->insert('tbl_order', $data);
        $order_id = $this->db->insert_id();
        $ssdata = array();
        $ssdata['order_id'] = $order_id;
        $this->session->set_userdata($ssdata);
        $total_content = $this->cart->contents();
        foreach ($total_content as $v_con) {
            $odata = array();
            $odata['order_id'] = $this->session->userdata('order_id');
            $odata['p_id'] = $v_con['id'];
            $odata['product_name'] = $v_con['name'];
            $odata['product_price'] = $v_con['price'];
            $odata['product_sales_quantity'] = $v_con['qty'];
            $this->db->insert('tbl_order_details', $odata);
        }
        $sql = "UPDATE tbl_product as p,tbl_order_details as d SET p.product_quantity=p.product_quantity-d.product_sales_quantity where p.p_id=d.p_id AND d.order_id=$order_id";
        $this->db->query($sql);
        $this->cart->destroy();
    }

    public function select_all_active_employee_for_invoice() {
        $this->db->select('*');
        $this->db->from('tbl_employee');
        $this->db->where('status', 1);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

}
