<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of checkout
 *
 * @author Rashed khan
 */
class checkout extends CI_Controller {

    public function index() {
//        $data = array();
//        $data['select_custo'] = $this->Admin_model->select_all_customer();
//        $data['body'] = $this->load->view('checkout', $data, true);
//        $data['sidebar'] = $this->load->view('menu', '', true);
//        $data['main_content'] = $this->load->view('order', $data, true);
//        $this->load->view('master', $data);
    }

    public function select_customer() {

        $customer_id = $this->input->post('c_id', true);
        $c_id = $this->checkout_model->select_cust($customer_id);
        if (!$c_id) {
            $data = array();
            $data['msg'] = "Invalid Customer ID";
            $this->session->set_userdata($data);
            redirect('admin/order');
        } else {
            $sdata = array();
            $sdata['c_id'] = $c_id->c_id;
            $this->session->set_userdata($sdata);
            redirect('cart/show_cart');
        }
    }

    public function payment() {
        $data = array();
        $data['main_content'] = $this->load->view('payment_form', $data, true);
        $this->load->view('master', $data);
    }

    public function save_invoice() {
        $data = array();
        $data['slect_active_employee'] = $this->checkout_model->select_all_active_employee_for_invoice();
        $grand_total = $this->input->post('g_t', true);
        $dis = $this->input->post('discount', true);
        $vat = $this->input->post('vat', true);
        $sdata = array();
        $sdata['g_t'] = $grand_total;
        $sdata['dis'] = $dis;
        $sdata['vat'] = $vat;
        $this->session->set_userdata($sdata);
        $data['main_content'] = $this->load->view('payment_form', $data, true);
        $this->load->view('master', $data);
    }

    public function confirm_order() {
        $payment_order = $this->input->post('payment_type', true);
        $pdata = array();
        $pdata['payment_type'] = $payment_order;
        $pdata['cash'] = $this->input->post('cash', true);
        $pdata['bank'] = $this->input->post('bank', true);
        $pdata['emp_id'] = $this->input->post('emp_id', true);
        $this->checkout_model->save_payment_info($pdata);
        $this->checkout_model->save_order_info();
        $this->session->unset_userdata('c_id');
        $this->session->unset_userdata('g_t');
        $this->session->unset_userdata('dis');
        $this->session->unset_userdata('vat');
        $order_id = $this->session->userdata('order_id');
        redirect('admin/view_invoice_details/' . $order_id);
        $this->session->unset_userdata('order_id');
    }

//    public function confirm_payment() {
//        $pdata = array();
//        $payment_order = $this->input->post('payment_type', true);
//        $pdata = array();
//        $pdata['payment_type'] = $payment_order;
//        $pdata['cash'] = $this->input->post('cash', true);
//        $pdata['bank'] = $this->input->post('bank', true);
//        $this->checkout_model->save_payment_info($pdata);
//        $this->session->unset_userdata('c_id');
//        $this->cart->destroy();
//        redirect('checkout/confirm');
//    }

    public function confirm() {
        $data = array();
        $data['body'] = $this->load->view('corfirm_success', '', true);
        $data['sidebar'] = $this->load->view('menu', '', true);
        $totaldata['main_content'] = $this->load->view('order', $data, true);
        $this->load->view('master', $totaldata);
    }

}
