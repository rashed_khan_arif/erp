<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class admin extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $admin_id = $this->session->userdata('admin_id');
        if ($admin_id == NULL) {
            redirect('welcome', 'refresh');
        }
    }

    public function index() {
        $data = array();
        $data['main_content'] = $this->load->view('home', $data, true);
        $this->load->view('master', $data);
    }

    public function debug($data) {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        exit();
    }

    /* Employee Information start */

    public function employee() {
        $data = array();
        $data['view_em'] = $this->Admin_model->view_employee_info();
        $data['main_content'] = $this->load->view('view_employee', $data, true);
        $this->load->view('master', $data);
    }

    public function add_employee() {
        $data = array();
        $data['main_content'] = $this->load->view('add_employee', $data, true);
        $this->load->view('master', $data);
    }

    public function view_employee() {
        $data = array();
        $data['view_em'] = $this->Admin_model->view_employee_info();
        $data['main_content'] = $this->load->view('view_employee', $data, true);
        $this->load->view('master', $data);
    }

    public function edit_employee($em_id) {
        $data = array();
        $data['view_en_em'] = $this->Admin_model->view_en_employee_info($em_id);
        $data['view_em'] = $this->Admin_model->view_employee_info();
        $data['main_content'] = $this->load->view('edit_employee', $data, true);
        $this->load->view('master', $data);
    }

    public function print_employee($em_id) {
        $data = array();
        $data['view_en_em'] = $this->Admin_model->view_en_employee_info($em_id);
        $this->load->view('print_employee_report', $data);
    }

    public function view_en_employee($em_id) {
        $data = array();
        $data['view_en_em'] = $this->Admin_model->view_en_employee_info($em_id);

        $data['main_content'] = $this->load->view('view_en_employee', $data, true);
        $this->load->view('master', $data);
    }

    public function inactive_employee() {
        $data = array();
        $data['view_em'] = $this->Admin_model->view_inactive_employee_info();
        $data['main_content'] = $this->load->view('inactive_employee', $data, true);
        $this->load->view('master', $data);
    }

    public function save_employee() {
        $data = array();
        $data['employee_id'] = $this->input->post('employee_id', TRUE);
        $data['first_name'] = $this->input->post('first_name', TRUE);
        $data['last_name'] = $this->input->post('last_name', TRUE);
        $data['designation'] = $this->input->post('designation', TRUE);
        $data['job_status'] = $this->input->post('job_status', TRUE);
        $data['job_status_info'] = $this->input->post('job_status_info', TRUE);
        $data['start_date'] = $this->input->post('start_date', TRUE);
        $data['end_date'] = $this->input->post('end_date', TRUE);
        $data['doj'] = $this->input->post('doj', TRUE);
        $data['dob'] = $this->input->post('dob', TRUE);
        $data['bank'] = $this->input->post('bank', TRUE);
        $data['present_address'] = $this->input->post('present_address', TRUE);
        $data['permenent_address'] = $this->input->post('permenent_address', TRUE);
        $data['identification'] = $this->input->post('identification', TRUE);
        $data['contact'] = $this->input->post('contact', TRUE);
        $data['em_contact'] = $this->input->post('em_contact', TRUE);
        $data['email'] = $this->input->post('email', TRUE);
        $data['status'] = $this->input->post('status', TRUE);
        $data['blood_group'] = $this->input->post('blood_group', TRUE);
        $data['gender'] = $this->input->post('gender', TRUE);
        $data['experience'] = $this->input->post('experience', TRUE);
        $data['my'] = $this->input->post('my', TRUE);
        $data['qualification'] = $this->input->post('qualification', TRUE);
        /* image_upload start */
        $config['upload_path'] = './emp_image/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 300;
        $config['max_width'] = 1024;
        $config['encrypt_name'] = true;
        $config['max_height'] = 768;
        $this->load->library('upload', $config);
        $error = '';
        $fdata = array();
        if (!$this->upload->do_upload('image')) {
            $error = $this->upload->display_errors();
            $rdata = array();
            $rdata['msg'] = $error;
            $this->session->set_userdata($rdata);
            redirect('admin/add_employee');
        } else {
            $fdata = $this->upload->data();
            $this->resize($config['upload_path'], $fdata['file_name']);
            $data['image'] = $config['upload_path'] . $fdata['file_name'];
//$this->debug($data);
            $this->Admin_model->save_employee_information($data);
            $sdata = array();
            $sdata['msg'] = 'success';
            $this->session->set_userdata($sdata);
            redirect('admin/add_employee');
        }
    }

    public function resize($path, $file) {
        $config['image_library'] = 'gd2';
        $config['source_image'] = $path;
        $config['create_thumb'] = true;
        $config['maintain_ratio'] = TRUE;
        $config['new_image'] = './uploads/' . $file;
        $config['width'] = 880;
        $config['height'] = 600;
        $this->load->library('image_lib', $config);
        $this->image_lib->resize();
    }

    /* image_upload send */

    public function search_employee() {
        $data = array();
        $search_text = $this->input->get('search');
        if ($search_text == null) {
            $rdata = array();
            $rdata['msg'] = "Please Enter Employee ID or Employee Name to Search";
            $this->session->set_userdata($rdata);
        }
        $data['view_em'] = $this->Admin_model->view_employee_info_by_value($search_text);
        if (empty($data['view_em'])) {
            $rdata = array();
            $rdata['msg'] = "No Result Found For" . "<span style='color:green'>" . " " . $search_text . "</span>";
            $this->session->set_userdata($rdata);
            $data['body'] = $this->load->view('search_emp', $data, true);
            $data['sidebar'] = $this->load->view('menu', '', true);
            $totaldata['main_content'] = $this->load->view('employee', $data, true);
            $this->load->view('master', $totaldata);
        } else {

            $data['main_content'] = $this->load->view('search_emp', $data, true);
            $this->load->view('master', $data);
        }
    }

    public function published_employee($emp_id) {
        $this->Admin_model->active_employee_info($emp_id);
        redirect('admin/view_employee');
    }

    public function un_published_employee($emp_id) {
        $this->Admin_model->un_published_employee_info($emp_id);
        redirect('admin/view_employee');
    }

    public function active_employee($empp_id) {
        $this->Admin_model->published_inactive_info($empp_id);
        redirect('admin/inactive_employee');
    }

    public function inactive_employee_info($empp_id) {
        $this->Admin_model->un_published_inactive_info($empp_id);
        redirect('admin/inactive_employee');
    }

    public function update_employee() {
        $data = array();
        $emp_id = $this->input->post('emp_id', TRUE);
        $data['employee_id'] = $this->input->post('employee_id', TRUE);
        $data['first_name'] = $this->input->post('first_name', TRUE);
        $data['last_name'] = $this->input->post('last_name', TRUE);
        $data['designation'] = $this->input->post('designation', TRUE);
        $data['job_status'] = $this->input->post('job_status', TRUE);
        $data['job_status_info'] = $this->input->post('job_status_info', TRUE);
        $data['start_date'] = $this->input->post('start_date', TRUE);
        $data['end_date'] = $this->input->post('end_date', TRUE);
        $data['doj'] = $this->input->post('doj', TRUE);
        $data['dob'] = $this->input->post('dob', TRUE);
        $data['bank'] = $this->input->post('bank', TRUE);
        $data['present_address'] = $this->input->post('present_address', TRUE);
        $data['permenent_address'] = $this->input->post('permenent_address', TRUE);
        $data['identification'] = $this->input->post('identification', TRUE);
        $data['contact'] = $this->input->post('contact', TRUE);
        $data['em_contact'] = $this->input->post('em_contact', TRUE);
        $data['email'] = $this->input->post('email', TRUE);
        $data['status'] = $this->input->post('status', TRUE);
        $data['blood_group'] = $this->input->post('blood_group', TRUE);
        $data['gender'] = $this->input->post('gender', TRUE);
        $data['experience'] = $this->input->post('experience', TRUE);
        $data['my'] = $this->input->post('my', TRUE);
        $data['qualification'] = $this->input->post('qualification', TRUE);
        $image = $this->Admin_model->select_pre_image($emp_id);
        /* image_upload start */
        $config['upload_path'] = './emp_image/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 300;
        $config['max_width'] = 1024;
        $config['encrypt_name'] = true;
        $config['max_height'] = 768;
        $this->load->library('upload', $config);
        $fdata = array();
        if (!$this->upload->do_upload('image')) {
            $data['image'] = $image->image;
            $sdata = array();
            $sdata['msg'] = 'Update success';
            $this->session->set_userdata($sdata);
            $this->Admin_model->update_employee_information($emp_id, $data);
            redirect('admin/edit_employee/' . $emp_id);
        } else {
            unlink($image->image);
            $fdata = $this->upload->data();
            $this->resize($config['upload_path'], $fdata['file_name']);
            $data['image'] = $config['upload_path'] . $fdata['file_name'];
            $this->Admin_model->update_employee_information($emp_id, $data);
            $sdata = array();
            $sdata['msg'] = 'Update success';
            $this->session->set_userdata($sdata);
            redirect('admin/edit_employee/' . $emp_id);
        }
    }

    public function delete_employee($id) {
        $this->db->where('emp_id', $id);
        $this->db->delete('tbl_employee');
        redirect('admin/view_employee');
    }

    /* Employee Information end */
    /* Salary Information start */

    public function salary() {
        $data = array();
        $data['employee_salary'] = $this->Admin_model->view_employee_salary_info();
        $tdata['main_content'] = $this->load->view('view_salary', $data, true);
        $this->load->view('master', $tdata);
    }

    public function single_salary($id) {
        $data = array();
        $data['employee'] = $this->Admin_model->view_employee_info_by_id($id);
        $data['employee_salary'] = $this->Admin_model->view_single_employee_salary_info($id);
        $data['employee_pay_salary'] = $this->Admin_model->view_single_employee_pay_salary_info($id);
        $data['main_content'] = $this->load->view('view_employee_salary', $data, true);
        $this->load->view('master', $data);
    }

    public function add_salary($id) {
        $data = array();
        $data['employee'] = $this->Admin_model->view_employee_info_by_id($id);
        $data['main_content'] = $this->load->view('add_salary', $data, true);
        $this->load->view('master', $data);
    }

    public function save_salary() {
        $data = array();
        $emp_id = $this->input->post('emp_id', TRUE);
        $data['emp_id'] = $emp_id;
        $data['month'] = $this->input->post('month', TRUE);
        $data['year'] = $this->input->post('year', TRUE);
        $data['basic_salary'] = $this->input->post('basic_salary', TRUE);
        $data['increment'] = $this->input->post('increment', TRUE);
        $data['house_rent'] = $this->input->post('house_rent', TRUE);
        $data['conveyance'] = $this->input->post('conveyance', TRUE);
        $data['medical'] = $this->input->post('medical', TRUE);
        $data['gross'] = $this->input->post('gross', TRUE);
        $data['overtime'] = $this->input->post('overtime', TRUE);
        $data['mobile'] = $this->input->post('mobile', TRUE);
        $data['arrear'] = $this->input->post('arrear', TRUE);
        $data['others'] = $this->input->post('others', TRUE);
        $data['bonus'] = $this->input->post('bonus', TRUE);
        $data['annual'] = $this->input->post('annual', TRUE);
        $data['income_tax'] = $this->input->post('income_tax', TRUE);
        $data['deduction'] = $this->input->post('deduction', TRUE);
        $data['total'] = $this->input->post('total', TRUE);
        $month = $this->input->post('month', TRUE);
        $year = $this->input->post('year', TRUE);
        $check = $this->Admin_model->check_employee($month, $year, $emp_id);
        if (!$check) {
            $this->Admin_model->save_salary_information($data);
            $sdata = array();
            $sdata['msg'] = 'Save success';
            $this->session->set_userdata($sdata);
            redirect('admin/salary_entry');
        } else {
            $ddata = array();
            $ddata['msg'] = 'Sorry Month or Year Already Exits';
            $this->session->set_userdata($ddata);
            redirect('admin/salary_entry');
        }
    }

    public function update_salary() {
        $data = array();
        $si_id = $this->input->post('si_id', TRUE);
        $emp_id = $this->input->post('emp_id', TRUE);
        $data['emp_id'] = $emp_id;
        $data['basic_salary'] = $this->input->post('basic_salary', TRUE);
        $data['month'] = $this->input->post('month', TRUE);
        $data['year'] = $this->input->post('year', TRUE);
        $data['increment'] = $this->input->post('increment', TRUE);
        $data['house_rent'] = $this->input->post('house_rent', TRUE);
        $data['conveyance'] = $this->input->post('conveyance', TRUE);
        $data['medical'] = $this->input->post('medical', TRUE);
        $data['gross'] = $this->input->post('gross', TRUE);
        $data['overtime'] = $this->input->post('overtime', TRUE);
        $data['mobile'] = $this->input->post('mobile', TRUE);
        $data['arrear'] = $this->input->post('arrear', TRUE);
        $data['others'] = $this->input->post('others', TRUE);
        $data['bonus'] = $this->input->post('bonus', TRUE);
        $data['annual'] = $this->input->post('annual', TRUE);
        $data['income_tax'] = $this->input->post('income_tax', TRUE);
        $data['deduction'] = $this->input->post('deduction', TRUE);
        $data['total'] = $this->input->post('total', TRUE);
        $month = $this->input->post('month', TRUE);
        $year = $this->input->post('year', TRUE);
        $check = $this->Admin_model->check_employee($month, $year, $emp_id);
        if (!$check) {
            $this->Admin_model->update_salary_information($si_id, $data);
            $sdata = array();
            $sdata['msg'] = 'Update success';
            $this->session->set_userdata($sdata);
            redirect('admin/salary');
        } else {
            $ddata = array();
            $ddata['msg'] = 'Sorry Month or Year Already Exits';
            $this->session->set_userdata($ddata);
            redirect('admin/salary');
        }
    }

    public function salary_entry() {
        $data = array();
        $data['employee'] = $this->Admin_model->view_employee_info();
        $data['main_content'] = $this->load->view('salary_entry', $data, true);
        $this->load->view('master', $data);
    }

    public function salary_payment() {
        $data = array();
        $data['unpaid_employee'] = $this->Admin_model->view_unpaid_employee_info();
        $data['main_content'] = $this->load->view('salary_payment', $data, true);
        $this->load->view('master', $data);
    }

    public function edit_salary($id) {
        $data = array();
        $data['all_salary_by_id'] = $this->Admin_model->select_all_salary_by_id($id);
        $data['main_content'] = $this->load->view('edit_salary', $data, true);
        $this->load->view('master', $data);
    }

    public function print_employee_salary_info($id) {
        $data = array();
        $data['employee'] = $this->Admin_model->view_employee_info_by_id($id);
        $data['employee_salary'] = $this->Admin_model->view_single_employee_salary_info($id);
        $this->load->view('print_salary_info', $data);
    }

    public function create_emp_salary_invoice($id) {
        $data['employee'] = $this->Admin_model->view_employee_info_by_id($id);
        $data['employee_salary'] = $this->Admin_model->view_single_employee_salary_info($id);
        $this->load->helper('dompdf');
        $view_file = $this->load->view('emp_salary_inv', $data, true);
        $print_file = pdf_create($view_file, 'INV_00' . $data['employee']->emp_id);
        echo $print_file;
    }

    public function pay_salary($si_id, $emp_id) {
        $data = array();
        $data['select_employee_month'] = $this->Admin_model->select_employee_month_year_info($emp_id);
        $data['select_employee'] = $this->Admin_model->select_employee_info($si_id, $emp_id);
        $data['main_content'] = $this->load->view('pay_salary', $data, true);
        $this->load->view('master', $data);
    }

    public function save_salary_payment() {
        $data = array();
        $si_id = $this->input->post('si_id', TRUE);
        $data['emp_id'] = $this->input->post('emp_id', TRUE);
        $data['si_id'] = $si_id;
        $data['month'] = $this->input->post('month', TRUE);
        $data['year'] = $this->input->post('year', TRUE);
        $data['cash'] = $this->input->post('cash', TRUE);
        $data['bank'] = $this->input->post('bank', TRUE);
        $data['note'] = $this->input->post('note', TRUE);
        $data['payment_type'] = $this->input->post('payment_type', TRUE);
        $data['payment_date'] = $this->input->post('payment_date', TRUE);
        //$this->debug($data);
        $this->Admin_model->save_salary_payment_info($data);
        $sdata = array();
        $sdata['msg'] = "Successfully payment";
        $this->session->set_userdata($sdata);
        redirect('admin/salary_payment');
    }

    public function delete_salary($id) {
        $this->Admin_model->delete_salary_info($id);
        redirect('admin/salary');
    }

    /* Salary Information end */
    /* asset Information start */

    public function asset() {
        $data = array();
        $data['all_item'] = $this->Admin_model->select_all_item();
        $data['main_content'] = $this->load->view('view_item', $data, true);
        $this->load->view('master', $data);
    }

    public function view_item() {
        $data = array();
        $data['all_item'] = $this->Admin_model->select_all_item();
        $data['main_content'] = $this->load->view('view_item', $data, true);
        $this->load->view('master', $data);
    }

    public function print_item($id) {
        $data = array();
        $data['all_item_by_id'] = $this->Admin_model->select_all_item_by_id($id);
        $this->load->view('print_item', $data);
    }

    public function create_invoice($id) {
        $data = array();
        $data['all_item_by_id'] = $this->Admin_model->select_all_item_by_id($id);
        $this->load->helper('dompdf');
        $view_file = $this->load->view('invoice', $data, true);
        $print_file = pdf_create($view_file, 'INV_00' . $data['all_item_by_id']->item_id);
        echo $print_file;
    }

    public function delete_item($id) {
        $data = array();
        $this->Admin_model->delete_items($id);
        redirect('admin/view_item');
    }

    public function edit_item($id) {
        $data = array();
        $data['all_item_by_id'] = $this->Admin_model->select_all_item_by_id($id);
        $data['main_content'] = $this->load->view('edit_item', $data, true);
        $this->load->view('master', $data);
    }

    public function view_items($id) {
        $data = array();
        $data['all_item_by_id'] = $this->Admin_model->select_all_item_by_id($id);
        $data['main_content'] = $this->load->view('item_details', $data, true);
        $this->load->view('master', $data);
    }

    public function add_new_item() {
        $data = array();
        $data['main_content'] = $this->load->view('add_new_item', $data, true);
        $this->load->view('master', $data);
    }

    public function save_item() {
        $data = array();
        $data['item_id'] = $this->input->post('item_id', TRUE);
        $data['category'] = $this->input->post('category', TRUE);
        $data['sub_category'] = $this->input->post('sub_category', TRUE);
        $data['description'] = $this->input->post('description', TRUE);
        $data['quantity'] = $this->input->post('quantity', TRUE);
        $data['type'] = $this->input->post('type', TRUE);
        $data['price_per_quantity'] = $this->input->post('price_per_quantity', TRUE);
        $data['invoice_no'] = $this->input->post('invoice_no', TRUE);
        $data['invoice_details_location'] = $this->input->post('invoice_details_location', TRUE);
        $data['date_of_purchase'] = $this->input->post('date_of_purchase', TRUE);
        $data['purchase_from'] = $this->input->post('purchase_from', TRUE);
        $data['reference'] = $this->input->post('reference', TRUE);
        $data['supplier'] = $this->input->post('supplier', TRUE);
        $data['contact_number'] = $this->input->post('contact_number', TRUE);
        $data['email'] = $this->input->post('email', TRUE);
        $sdata = array();
        $sdata['msg'] = "Successfully Saved";
        $this->session->set_userdata($sdata);
        $this->Admin_model->save_items($data);
        redirect('admin/add_new_item');
    }

    public function update_item() {
        $data = array();
        $item_no = $this->input->post('item_no', TRUE);
        $data['item_id'] = $this->input->post('item_id', TRUE);
        $data['category'] = $this->input->post('category', TRUE);
        $data['sub_category'] = $this->input->post('sub_category', TRUE);
        $data['description'] = $this->input->post('description', TRUE);
        $data['quantity'] = $this->input->post('quantity', TRUE);
        $data['type'] = $this->input->post('type', TRUE);
        $data['price_per_quantity'] = $this->input->post('price_per_quantity', TRUE);
        $data['invoice_no'] = $this->input->post('invoice_no', TRUE);
        $data['invoice_details_location'] = $this->input->post('invoice_details_location', TRUE);
        $data['date_of_purchase'] = $this->input->post('date_of_purchase', TRUE);
        $data['purchase_from'] = $this->input->post('purchase_from', TRUE);
        $data['reference'] = $this->input->post('reference', TRUE);
        $data['supplier'] = $this->input->post('supplier', TRUE);
        $data['contact_number'] = $this->input->post('contact_number', TRUE);
        $data['email'] = $this->input->post('email', TRUE);
        $sdata = array();
        $sdata['msg'] = "Successfully Updated";
        $this->session->set_userdata($sdata);
        $this->Admin_model->update_items($item_no, $data);
        redirect('admin/edit_item/' . $item_no);
    }

    public function search_item() {
        $data = array();
        $search_text = $this->input->get('search');
        $data['view_em'] = $this->Admin_model->view_item_info_by_value($search_text);
        if (empty($data['view_em'])) {
            $rdata = array();
            $rdata['msg'] = "No Result Found For" . "<span style='color:green'>" . " " . $search_text . "</span>";
            $this->session->set_userdata($rdata);
            $data['body'] = $this->load->view('search_item', $data, true);
            $data['sidebar'] = $this->load->view('menu', '', true);
            $totaldata['main_content'] = $this->load->view('employee', $data, true);
            $this->load->view('master', $totaldata);
        } else {
            $data['main_content'] = $this->load->view('search_item', $data, true);
            $this->load->view('master', $data);
        }
    }

    public function search_customer() {
        $data = array();
        $search_text = $this->input->get('search');
        $data['view_em'] = $this->Admin_model->view_custo_info_by_value($search_text);
        if (empty($data['view_em'])) {
            $rdata = array();
            $rdata['msg'] = "No Result Found For" . "<span style='color:green'>" . " " . $search_text . "</span>";
            $this->session->set_userdata($rdata);
            $data['body'] = $this->load->view('search_customer', $data, true);
            $data['sidebar'] = $this->load->view('menu', '', true);
            $totaldata['main_content'] = $this->load->view('employee', $data, true);
            $this->load->view('master', $totaldata);
        } else {
            $data['main_content'] = $this->load->view('search_customer', $data, true);
            $this->load->view('master', $data);
        }
    }

    /* asset Information end */

    public function customer() {
        $data = array();
        $data['select_custo'] = $this->Admin_model->select_all_customer();
        $data['main_content'] = $this->load->view('view_customer', $data, true);
        $this->load->view('master', $data);
    }

    public function view_customer() {
        $data = array();
        $data['select_custo'] = $this->Admin_model->select_all_customer();
        $data['main_content'] = $this->load->view('view_customer', $data, true);
        $this->load->view('master', $data);
    }

    public function view_customers($id) {
        $data = array();
        $data['select_customer'] = $this->Admin_model->select_all_customer_id($id);
        $data['main_content'] = $this->load->view('customer_details', $data, true);
        $this->load->view('master', $data);
    }

    public function add_customer() {
        $data = array();
        $data['main_content'] = $this->load->view('add_customer', $data, true);
        $this->load->view('master', $data);
    }

    public function save_customer() {
        $data['customer_id'] = $this->input->post('customer_id', TRUE);
        $data['organization_name'] = $this->input->post('organization_name', TRUE);
        $data['reference'] = $this->input->post('reference', TRUE);
        $data['date_of_join'] = $this->input->post('date_of_join', TRUE);
        $data['address'] = $this->input->post('address', TRUE);
        $data['phone'] = $this->input->post('phone', TRUE);
        $data['email'] = $this->input->post('email', TRUE);
        $data['credit'] = $this->input->post('credit', TRUE);
        $data['credit_amount'] = $this->input->post('credit_amount', TRUE);
        $data['shipment_address'] = $this->input->post('shipment_address', TRUE);
        $this->Admin_model->save_customer_info($data);
        $rdata = array();
        $rdata['msg'] = "Customer Information Saved";
        $this->session->set_userdata($rdata);
        redirect('admin/add_customer');
    }

    public function print_customer($id) {
        $data = array();
        $data['select_customer'] = $this->Admin_model->select_all_customer_id($id);
        $this->load->view('print_cust', $data);
    }

    public function create_cust_invoice($id) {
        $data = array();
        $data['select_customer'] = $this->Admin_model->select_all_customer_id($id);
        $this->load->helper('dompdf');
        $view_file = $this->load->view('cust_invoice', $data, true);
        $print_file = pdf_create($view_file, 'INV_00' . $data['select_customer']->c_id);
        echo $print_file;
    }

    public function edit_customer($id) {
        $data = array();
        $data['select_customer'] = $this->Admin_model->select_all_customer_id($id);
        $data['main_content'] = $this->load->view('edit_customer', $data, true);
        $this->load->view('master', $data);
    }

    public function update_customer() {
        $c_id = $this->input->post('c_id', TRUE);
        $data['customer_id'] = $this->input->post('customer_id', TRUE);
        $data['organization_name'] = $this->input->post('organization_name', TRUE);
        $data['reference'] = $this->input->post('reference', TRUE);
        $data['date_of_join'] = $this->input->post('date_of_join', TRUE);
        $data['address'] = $this->input->post('address', TRUE);
        $data['phone'] = $this->input->post('phone', TRUE);
        $data['email'] = $this->input->post('email', TRUE);
        $data['credit'] = $this->input->post('credit', TRUE);
        $data['credit_amount'] = $this->input->post('credit_amount', TRUE);
        $data['shipment_address'] = $this->input->post('shipment_address', TRUE);
        $this->Admin_model->update_customer_info($c_id, $data);
        $rdata = array();
        $rdata['msg'] = "Customer Information Updated";
        $this->session->set_userdata($rdata);
        redirect('admin/edit_customer/' . $c_id);
    }

    public function delete_customer($id) {
        $data = array();
        $this->Admin_model->delete_customers($id);
        redirect('admin/view_customer');
    }

    public function storein() {
        $data = array();
        $data['select_product'] = $this->Admin_model->select_all_product();
        $data['main_content'] = $this->load->view('view_sotrein', $data, true);
        $this->load->view('master', $data);
    }

    public function view_product($id) {
        $data = array();
        $data['select_product_id'] = $this->Admin_model->select_all_product_id($id);
        $data['main_content'] = $this->load->view('product_detalis', $data, true);
        $this->load->view('master', $data);
    }

    public function add_product() {
        $data = array();
        $data['main_content'] = $this->load->view('add_new_product', $data, true);
        $this->load->view('master', $data);
    }

    public function save_product() {
        $data['product_id'] = $this->input->post('product_id', TRUE);
        $data['product_name'] = $this->input->post('product_name', TRUE);
        $data['product_category'] = $this->input->post('product_category', TRUE);
        $data['product_sub_category'] = $this->input->post('product_sub_category', TRUE);
        $data['product_description'] = $this->input->post('product_description', TRUE);
        $data['product_quantity'] = $this->input->post('product_quantity', TRUE);
        $data['product_price_per_quantity'] = $this->input->post('product_price_per_quantity', TRUE);
        $data['discount'] = $this->input->post('discount', TRUE);
        $data['rdiscount'] = ($data['product_price_per_quantity'] * $data['discount']) / 100;
        $data['price_after_discount'] = ($data['product_price_per_quantity'] - $data['rdiscount']);
        $data['vat'] = $this->input->post('vat', TRUE);
        $data['rvat'] = ($data['price_after_discount'] * $data['vat']) / 100;
        $data['price_after_vat'] = ($data['price_after_discount'] + $data['rvat']);
        $data['price'] = ( $data['price_after_discount'] * $data['product_quantity']);
        $data['storing_date'] = $this->input->post('storing_date', TRUE);
        $data['storing_location'] = $this->input->post('storing_location', TRUE);
        $this->Admin_model->save_product_info($data);
        $rdata = array();
        $rdata['msg'] = "Product Information Saved";
        $this->session->set_userdata($rdata);
        redirect('admin/add_product');
    }

    public function print_product($id) {
        $data = array();
        $data['select_product_id'] = $this->Admin_model->select_all_product_id($id);
        $this->load->view('print_product', $data);
    }

    public function product_invoice($id) {
        $data = array();
        $data['select_product_id'] = $this->Admin_model->select_all_product_id($id);
        $this->load->helper('dompdf');
        $view_file = $this->load->view('product_invoice', $data, true);
        $print_file = pdf_create($view_file, 'INV_00' . $data['select_product_id']->p_id);
        echo $print_file;
    }

    public function edit_product($id) {
        $data = array();
        $data['select_product_id'] = $this->Admin_model->select_all_product_id($id);
        $data['main_content'] = $this->load->view('edit_product', $data, true);
        $this->load->view('master', $data);
    }

    public function update_product() {
        $p_id = $this->input->post('p_id', TRUE);
        $data['product_id'] = $this->input->post('product_id', TRUE);
        $data['product_name'] = $this->input->post('product_name', TRUE);
        $data['product_category'] = $this->input->post('product_category', TRUE);
        $data['product_sub_category'] = $this->input->post('product_sub_category', TRUE);
        $data['product_description'] = $this->input->post('product_description', TRUE);
        $data['product_quantity'] = $this->input->post('product_quantity', TRUE);
        $data['product_price_per_quantity'] = $this->input->post('product_price_per_quantity', TRUE);
        $data['discount'] = $this->input->post('discount', TRUE);
        $data['rdiscount'] = $this->input->post('rdiscount', TRUE);
        $data['price_after_discount'] = $this->input->post('price_after_discount', TRUE);
        $data['vat'] = $this->input->post('vat', TRUE);
        $data['rvat'] = $this->input->post('rvat', TRUE);
        $data['price_after_vat'] = $this->input->post('price_after_vat', TRUE);
        $data['price'] = $this->input->post('price', TRUE);
        $data['storing_date'] = $this->input->post('storing_date', TRUE);
        $data['storing_location'] = $this->input->post('storing_location', TRUE);
        $this->Admin_model->update_product_info($p_id, $data);
        $rdata = array();
        $rdata['msg'] = "Product Information Updated";
        $this->session->set_userdata($rdata);
        redirect('admin/edit_product/' . $p_id);
    }

    public function search_product() {
        $data = array();
        $search_text = $this->input->get('search');
        $data['select_product'] = $this->Admin_model->view_product_info_by_value($search_text);
        if (empty($data['select_product'])) {
            $rdata = array();
            $rdata['msg'] = "No Result Found For" . $search_text;
            $this->session->set_userdata($rdata);
            $data['main_content'] = $this->load->view('search_product', $data, true);
            $this->load->view('master', $data);
        } else {
            $data['main_content'] = $this->load->view('search_product', $data, true);
            $this->load->view('master', $data);
        }
    }

    public function search_product_for_invoice() {
        $data = array();
        $search_text = $this->input->get('search');
        $data['select_product'] = $this->Admin_model->view_product_info_by_value_id($search_text);
        if (empty($data['select_product'])) {
            $rdata = array();
            $rdata['msg'] = "No Result Found For" . "<span style='color:green'>" . " " . $search_text . "</span>";
            $this->session->set_userdata($rdata);
            $data['main_content'] = $this->load->view('search_product_inv', $data, true);
            $this->load->view('master', $totaldata);
        } else {
            $data['main_content'] = $this->load->view('search_product_inv', $data, true);
            $this->load->view('master', $data);
        }
    }

    public function search_order() {
        $data = array();
        $search_text = $this->input->get('search');
        $data['select_invoice'] = $this->Admin_model->view_invoice_info_by_value_id($search_text);
        if (empty($data['select_invoice'])) {
            $rdata = array();
            $rdata['msg'] = "No Result Found For" . $search_text;
            $this->session->set_userdata($rdata);
            redirect('admin/view_invoice');
        } else {
            $data['main_content'] = $this->load->view('search_order_inv', $data, true);
            $this->load->view('master', $data);
        }
    }

    /* Product Information end */

    public function order() {
        $c_id = $this->session->userdata('c_id');
        if ($c_id) {
            $rdata = array();
            $rdata['msg'] = "Please complete the previous Order";
            $this->session->set_userdata($rdata);
            redirect('admin/view_invoice');
        }
        $data = array();
        $data['select_product'] = $this->Admin_model->select_all_product();
        $data['main_content'] = $this->load->view('checkout', $data, true);
        $this->load->view('master', $data);
    }

    public function create_order() {
        $data = array();
        $data['select_product'] = $this->Admin_model->select_all_product();
        $data['main_content'] = $this->load->view('view_order', $data, true);
        $this->load->view('master', $data);
    }

    public function paid_order($id) {
        $data = array();
        $data['select_unpaid_order'] = $this->Admin_model->select_unpaid_order_info($id);
        $data['main_content'] = $this->load->view('paid_order', $data, true);
        $this->load->view('master', $data);
    }

    public function update_order() {
        $data = array();
        $payment_id = $this->input->post('payment_id', true);
        $cash = $this->input->post('cash', true);
        $data['check'] = $this->Admin_model->check_amount($payment_id);
        if ($data['check']->cash + $cash > $data['check']->order_total) {
            $rdata['msg'] = "Your Payment is gatter than your due ! Your Due is " . ($data['check']->order_total - $data['check']->cash);
            $this->session->set_userdata($rdata);
            redirect('admin/paid_order/' . $data['check']->order_id);
        } else {
            $this->Admin_model->update_order_payment($payment_id, $cash);
            $rdata = array();
            $rdata['msg'] = "Successfully Paid" . " " . $cash;
            $this->session->set_userdata($rdata);
            redirect('admin/view_invoice');
        }
    }

    public function view_invoice() {
        $data = array();
        $data['all_order'] = $this->Admin_model->select_all_order();
        $data['main_content'] = $this->load->view('view_invoice', $data, true);
        $this->load->view('master', $data);
    }

    public function unpaid_orders() {
        $data = array();
        $data['all_unpaid_order'] = $this->Admin_model->select_all_unpaid_order();
        $data['main_content'] = $this->load->view('unpaid_invoice', $data, true);
        $this->load->view('master', $data);
    }

    public function view_invoice_details($o_id) {
        error_reporting(0);
        $data = array();
        $data['order_info'] = $this->Admin_model->select_order_info($o_id);
        $data['order_full_details'] = $this->Admin_model->select_product_info($o_id);
        $this->load->view('invoice_details', $data);
    }

    public function invoice($o_id) {
        $data = array();
        $data['order_info'] = $this->Admin_model->select_order_info($o_id);
        $data['order_full_details'] = $this->Admin_model->select_product_info($o_id);
        $this->load->helper('dompdf');
        $view_file = $this->load->view('order_invoice', $data, true);
        $print_file = pdf_create($view_file, 'INV_00' . $data['order_info']->order_id);
        echo $print_file;
    }

    public function storeout() {
        $data = array();
        $data['sidebar'] = $this->load->view('menu', '', true);
        $data['main_content'] = $this->load->view('storeout', '', true);
        $this->load->view('master', $data);
    }

    public function payment() {
        $data = array();
        $data['sidebar'] = $this->load->view('menu', '', true);
        $data['main_content'] = $this->load->view('payment', '', true);
        $this->load->view('master', $data);
    }

    public function report() {
        $data = array();
        $data['sidebar'] = $this->load->view('menu', '', true);
        $data['main_content'] = $this->load->view('report', '', true);
        $this->load->view('master', $data);
    }

    public function logout_customer() {
        $this->session->unset_userdata('c_id');
        $this->session->unset_userdata('g_t');
        $this->session->unset_userdata('dis');
        $this->session->unset_userdata('vat');
        $this->session->unset_userdata('order_id');
        $this->cart->destroy();
        redirect('admin/view_invoice');
    }

    /*     * ***************** */

    public function logout() {
        $this->session->unset_userdata('admin_id');
        $this->session->unset_userdata('access_level');
        $this->session->unset_userdata('admin_name');
        redirect('welcome');
    }

}
