<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class welcome extends CI_Controller {
public function __construct() {
        parent::__construct();
        $admin_id = $this->session->userdata('admin_id');
        if ($admin_id != NULL) {
            redirect('admin', 'refresh');
        }
    }
    public function index() {
        $this->load->view('login');
    }

    public function check_login() {
        $email_address = $this->input->post('admin_email', TRUE);
        $pass = $this->input->post('password', true);
        $password = md5($pass);
        $result = $this->Admin_model->check_admin_info($email_address, $password);
        if (!$result) {
            if (empty($pass) || empty($email_address)) {
                $rdata = array();
                $rdata['message'] = 'Email Address Or Password Is empty ';
                $this->session->set_userdata($rdata);
                redirect('welcome');
            } else {
                $sdata = array();
                $sdata['message'] = 'Please Enter Valid Email Address';
                $this->session->set_userdata($sdata);
                redirect('welcome');
            }
        } else {
            $data = array();
            $data['admin_id'] = $result->admin_id;
            $data['admin_name'] = $result->admin_name;
            $data['access_level'] = $result->access;
            $this->session->set_userdata($data);
            redirect('admin');
        }
    }
 
}
