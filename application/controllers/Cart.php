<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Cart
 *
 * @author Rashed khan
 */
class cart extends CI_Controller {

    public function add_to_cart($id) {
        $qty = $this->input->post('qty', true);
        $product_info = $this->Admin_model->select_all_product_id($id);
        $data = array(
            'id' => $product_info->p_id,
            'qty' => $qty,
            'price' => $product_info->price_after_vat,
            'name' => $product_info->product_name,
            'discount' => $product_info->discount,
            'vat' => $product_info->vat,
        );

        $this->cart->insert($data);
        redirect('cart/show_cart');
    }

    public function show_cart() {
        $data = array();
        //$data['select_product'] = $this->Admin_model->select_all_product();
        $data['main_content'] = $this->load->view('show_cart', $data, true);
        $this->load->view('master', $data);
    }

      public function update_cart($rowid) {
        $data = array(
            'rowid' => $rowid,
            'qty' => $this->input->post('qty', true),
        );
        $this->cart->update($data);
        redirect('cart/show_cart');
    }
     public function delete_form_cart($rowid) {
        $data = array(
            'rowid' => $rowid,
            'qty' => 0,
        );
        $this->cart->update($data);
        redirect('cart/show_cart');
    }
}