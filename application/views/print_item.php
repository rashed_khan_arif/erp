<div class="panel panel-default" id="printarea">
    <div class="panel-body bk">
        <div class="row">
            <html>
                <head>
                    <link href="<?php echo base_url(); ?>asset/css/bootstrap.min.css" rel="stylesheet">
                    <link href="<?php echo base_url(); ?>asset/css/bootstrap.css" rel="stylesheet" type="text/css"/>
                    <link href="<?php echo base_url(); ?>asset/css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>   
                    <link href="<?php echo base_url(); ?>asset/css/style.css" rel="stylesheet" type="text/css"/>
                </head>
                <body>   
                    <div class="col-md-6 col-md-offset-3">

                        <fieldset>
                            <table class="table table-condensed table-hover table-striped table-bordered">

                                <tr class="info msg">
                                    <td colspan="2" class="text-center"><span style="font-size: 30px">Item Details</span></td>
                                </tr>

                                <tr class="info">
                                    <td><label for="fname" class="control-label">Item ID</label></td>
                                <input type="hidden" class="box" name="item_no" required id="fname">
                                <td><?php echo $all_item_by_id->item_no ?></td>
                                </tr>
                                <tr class="info">
                                    <td><label for="lname" class="control-label">Category</label></td>
                                    <td><?php echo $all_item_by_id->category ?></td>
                                </tr>
                                <tr class="info">
                                    <td><label for="bank" class="control-label">Sub category</label></td>
                                    <td><?php echo $all_item_by_id->sub_category ?></td>
                                </tr>
                                <tr class="info">
                                    <td><label for="dob" class="control-label">Description</label></td>
                                    <td>
                                        <?php echo $all_item_by_id->description ?>
                                    </td>
                                </tr>
                                <tr class="info">
                                    <td><label for="id" class="control-label">Quantity</label></td>
                                    <td><?php echo $all_item_by_id->quantity ?></td>
                                </tr>
                                <tr class="info">
                                    <td><label for="ross" class="control-label">Type</label></td>
                                    <td><?php echo $all_item_by_id->type ?></td>
                                </tr>
                                <tr class="info">
                                    <td><label for="em" class="control-label">Price per quantity</label></td>
                                    <td><?php echo $all_item_by_id->price_per_quantity ?></td>
                                </tr>
                                <tr class="info">
                                    <td><label for="email" class="control-label">Invoice no</label></td>
                                    <td><?php echo $all_item_by_id->invoice_no ?></td>
                                </tr>
                                <tr class="info">
                                    <td><label class="control-label">Invoice details location</label></td>
                                    <td><?php echo $all_item_by_id->invoice_details_location ?></td>
                                </tr>         
                                <tr class="info">
                                    <td><label class="control-label">Date of purchase</label></td>
                                    <td><?php echo $all_item_by_id->date_of_purchase ?></td>
                                </tr>
                                <tr class="info">
                                    <td><label for="doj" class="control-label">Purchase from</label></td>
                                    <td><?php echo $all_item_by_id->purchase_from ?></td>
                                </tr>
                                <tr class="info">
                                    <td><label for="ex" class="control-label">Reference</label></td>
                                    <td>
                                        <?php echo $all_item_by_id->reference ?>
                                    </td>
                                </tr>
                                <tr class="info">
                                    <td><label for="qf" class="control-label">Supplier</label></td>
                                    <td><?php echo $all_item_by_id->supplier ?></td>
                                </tr>
                                <tr class="info">
                                    <td><label for="dg" class="control-label">Contact number</label></td>
                                    <td><?php echo $all_item_by_id->contact_number ?></td>
                                </tr>
                                <tr class="info">
                                    <td><label for="dg" class="control-label">Email</label></td>
                                    <td> <?php echo $all_item_by_id->email ?></td>
                                </tr>

                                <script type="text/javascript">
                                    function printDi(printarea) {
                                        var printContents = document.getElementById(printarea).innerHTML;
                                        var originalContents = document.body.innerHTML;

                                        document.body.innerHTML = printContents;

                                        window.print();

                                        document.body.innerHTML = originalContents;
                                    }
                                </script>
                                <tr class="info">
                                    <td colspan="2">   
                                        <a href="" class="btn btn-info btn-block" onclick="printDi('printarea')"  id="print">Print</a>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </div>
        </div>
    </div>
</div>
</body>
</html>