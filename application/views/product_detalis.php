<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <a href="<?php echo base_url() ?>admin/storein" class="btn btn-info">View Product</a>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-8 col-md-offset-2" style="border: solid 1px #ccc">
                    <form action="<?php echo base_url() ?>admin/save_product" method="post">
                        <table class="table table-condensed table-hover table-striped table-bordered">
                            <?php
                            $msg = $this->session->userdata('msg');
                            if ($msg) {
                                ?>
                                <tr class="info msg" id="msg">
                                    <td colspan="2" class="text-center"><?php
                                        echo $msg;
                                        $this->session->unset_userdata('msg');
                                        ?></td>
                                </tr>
                            <?php } ?>
                       
                            <tr class="success">
                                <td colspan="2" class="text-center"><span style="font-size: 25px; font-weight: bold; font-family: sans-serif;">Product Details</span></td>
                            </tr>

                            <tr class="">
                                <td><label for="fname" class="control-label">Product ID</label></td>
                                <td><?php echo $select_product_id->product_id ?></td>
                            </tr>
                            <tr class="">
                                <td><label for="lname" class="control-label">Product Name</label></td>
                                <td><?php echo $select_product_id->product_name ?></td>
                            </tr>
                            <tr class="">
                                <td><label class="control-label">Product Category</label></td>
                                <td>
                                    <?php echo $select_product_id->product_category ?>
                                </td>
                            </tr>
                            <tr class="">
                                <td><label for="dob" class="control-label">Product Sub Category</label></td>
                                <td>
                                    <?php echo $select_product_id->product_sub_category ?>
                                </td>
                            </tr>
                            <tr class="">
                                <td><label for="id" class="control-label">Product Description</label></td>
                                <td>
                                    <?php echo $select_product_id->product_description ?>
                                </td>
                            </tr>
                            <tr class="">
                                <td><label for="ross" class="control-label">Product Quantity</label></td>
                                <td>
                                    <?php echo $select_product_id->product_quantity ?>  
                                </td>
                            </tr>
                            <tr class="">
                                <td><label for="em" class="control-label">Product Price Per Quantity</label></td>
                                <td>
                                    <?php echo $select_product_id->product_price_per_quantity ?>
                                </td>
                            </tr>
                            <tr class="">
                                <td><label for="email" class="control-label">Discount</label></td>
                                <td>
                                    <?php echo $select_product_id->discount ?> &nbsp;&nbsp;%                                    
                                </td>
                            </tr>
                            <tr class="">
                                <td><label for="email" class="control-label">Price After Discount</label></td>
                                <td>
                                    <?php echo $select_product_id->price_after_discount ?>  
                                </td>
                            </tr>
                            <tr class="">
                                <td><label for="doj" class="control-label">Vat</label></td>
                                <td>
                                    <?php echo $select_product_id->vat ?> &nbsp;&nbsp;%
                                </td>
                            </tr>           
                            <tr class="">
                                <td><label for="ex" class="control-label">Price After Vat</label></td>
                                <td>
                                    <?php echo $select_product_id->price_after_vat ?>  
                                </td>
                            </tr>
                            <tr class="">
                                <td><label for="qf" class="control-label">Price</label></td>
                                <td>
                                    <?php echo $select_product_id->price ?>
                                </td>
                            </tr>
                            <tr class="">
                                <td><label for="dg" class="control-label">Storing Date</label></td>
                                <td> 
                                    <?php echo $select_product_id->storing_date ?>
                                </td>
                            </tr>
                            <tr class="">
                                <td><label for="dg" class="control-label">Storing Location</label></td>
                                <td> 
                                    <?php echo $select_product_id->storing_location ?>
                                </td>
                            </tr>      

                            <tr class="">
                                <td colspan="2">
                                    <a href="<?php echo base_url() ?>admin/print_product/<?php echo $select_product_id->p_id; ?>" target="_" class="btn btn-info btn-block">Print Product Information</a>
                                    <a href="<?php echo base_url() ?>admin/product_invoice/<?php echo $select_product_id->p_id; ?>" class="btn btn-success btn-block">Save as Pdf</a>
                                    <a href="<?php echo base_url() ?>admin/edit_product/<?php echo $select_product_id->p_id; ?>" class="btn btn-block btn-default">Edit This Product</a>
                                </td>
                            </tr>
                        </table>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
