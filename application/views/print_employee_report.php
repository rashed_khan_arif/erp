<html>
    <head>
        <link href="<?php echo base_url(); ?>asset/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>asset/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>asset/css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>   
        <link href="<?php echo base_url(); ?>asset/css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="col-md-6 col-md-offset-3 border" id="printarea">
            <div class="row">
                <br/>
                <div class="col-md-4 text-center">
                    <span><br/><br/>
                        <?php echo $view_en_em->first_name . " " . $view_en_em->last_name ?></span><br/><br/>
                    COntact No:&nbsp;&nbsp;<?php echo $view_en_em->contact ?><br/>
                </div>
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <img src="<?php echo base_url() . $view_en_em->image ?>" class="img-thumbnail" width="150" height="150" alt="Employee image"/>
                </div>
            </div><br/><br/>
            <fieldset>
                <legend class="text-center cll">Personal Information</legend>
                <table class="table table-responsive table-bordered">
                    <thead>
                    <th class="text-center">ID</th>
                    <th class="text-center">D.O.B</th>
                    <th class="text-center">Account NO.</th>
                    <th class="text-center">Blood Group</th>
                    <th class="text-center">Gender</th>
                    </thead>
                    <tbody>
                        <tr class="text-center">
                            <td><?php echo $view_en_em->employee_id ?></td>
                            <td><?php echo $view_en_em->dob ?></td>
                            <td><?php echo $view_en_em->bank ?></td>
                            <td><?php echo $view_en_em->blood_group ?></td>
                            <td><?php echo $view_en_em->gender ?></td>
                        </tr>
                    </tbody>
                </table>
            </fieldset><br/>
            <fieldset>
                <legend class="text-center cll">Contact Information</legend>
                <table class="table table-responsive table-bordered">
                    <thead>  
                    <th class="text-center">Emergency Contact</th>
                    <th class="text-center">Present<br/> Address</th>
                    <th class="text-center">Permanent <br/> Address</th>
                    <th class="text-center">Identification</th>
                    <th class="text-center">Email No.</th>
                    </thead>
                    <tbody>
                        <tr class="text-center">
                            <td><?php echo $view_en_em->em_contact ?></td>
                            <td><textarea class="tarea" disabled=""><?php echo $view_en_em->present_address ?></textarea></td>
                            <td><textarea class="tarea" disabled=""><?php echo $view_en_em->permenent_address ?></textarea></td>
                            <td><?php echo $view_en_em->identification ?></td>
                            <td><?php echo $view_en_em->email ?></td>
                        </tr>
                    </tbody>
                </table>
            </fieldset><br/>
            <fieldset>
                <legend class="text-center cll">Job Details</legend>
                <table class="table table-responsive table-bordered">
                    <thead>  
                    <th class="text-center">Designation</th>
                    <th class="text-center">Job Status</th>
                    <th class="text-center">Joining Date</th>
                    <th class="text-center">Experience</th>
                    <th class="text-center">Qualification</th>
                    <th class="text-center">Status</th>
                    </thead>
                    <tbody>
                        <tr class="text-center">
                            <td><?php echo $view_en_em->designation ?></td>
                            <td><?php echo $view_en_em->job_status . "<br/>" . $view_en_em->job_status_info ?></td>
                            <td><?php echo $view_en_em->doj ?></td>
                            <td><?php echo $view_en_em->experience ?></td>
                            <td><?php echo $view_en_em->qualification ?></td>
                            <td><?php
                                if ($view_en_em->status == 1) {
                                    echo 'Active';
                                } else {
                                    echo "Inactive";
                                }
                                ?></td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>
            <br/><br/><br/>
            <div class="row">
                <script type="text/javascript">
                    function printDiv(printarea) {
                        var printContents = document.getElementById(printarea).innerHTML;
                        var originalContents = document.body.innerHTML;

                        document.body.innerHTML = printContents;

                        window.print();

                        document.body.innerHTML = originalContents;
                    }
                </script>

            </div>
            <br/>
        </div>
        <div class="col-md-6 col-md-offset-3 text-center">
            <a href="" target="_" class="btn btn-default" onclick="printDiv('printarea')"  id="print">Print Information</a>
        </div>
        <div class="col-md-6 col-md-offset-3">
         <br/><br/><br/><br/>
        </div>
        
    </body>
</html>