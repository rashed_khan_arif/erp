<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <a href="<?php echo base_url() ?>admin/view_item" class="btn btn-info">View item</a>
            <a href="<?php echo base_url() ?>admin/add_new_item" class="btn btn-info">Add New item</a>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-8 col-md-offset-2" style="border:  solid 1px #ccc">
                    <table class="table table-condensed table-hover table-striped table-bordered">
                        <?php
                        $msg = $this->session->userdata('msg');
                        if ($msg) {
                            ?>
                            <tr class="info msg" id="msg">
                                <td colspan="2" class="text-center"><?php
                                    echo $msg;
                                    $this->session->unset_userdata('msg');
                                    ?></td>
                            </tr>
                        <?php } ?>
                        <tr class="success msg">
                            <td colspan="2" class="text-center"><span style="font-size: 30px">Item Details</span></td>
                        </tr>

                        <tr class="">
                            <td><label for="fname" class="control-label">Item ID</label></td>
                        <input type="hidden" class="box" name="item_no" required id="fname">
                        <td><?php echo $all_item_by_id->item_no ?></td>
                        </tr>
                        <tr class="">
                            <td><label for="lname" class="control-label">Category</label></td>
                            <td><?php echo $all_item_by_id->category ?></td>
                        </tr>
                        <tr class="">
                            <td><label for="bank" class="control-label">Sub category</label></td>
                            <td><?php echo $all_item_by_id->sub_category ?></td>
                        </tr>
                        <tr class="">
                            <td><label for="dob" class="control-label">Description</label></td>
                            <td>
                                <?php echo $all_item_by_id->description ?>
                            </td>
                        </tr>
                        <tr class="">
                            <td><label for="id" class="control-label">Quantity</label></td>
                            <td><?php echo $all_item_by_id->quantity ?></td>
                        </tr>
                        <tr class="">
                            <td><label for="ross" class="control-label">Type</label></td>
                            <td><?php echo $all_item_by_id->type ?></td>
                        </tr>
                        <tr class="">
                            <td><label for="em" class="control-label">Price per quantity</label></td>
                            <td><?php echo $all_item_by_id->price_per_quantity ?></td>
                        </tr>
                        <tr class="">
                            <td><label for="email" class="control-label">Invoice no</label></td>
                            <td><?php echo $all_item_by_id->invoice_no ?></td>
                        </tr>
                        <tr class="">
                            <td><label class="control-label">Invoice details location</label></td>
                            <td><?php echo $all_item_by_id->invoice_details_location ?></td>
                        </tr>         
                        <tr class="">
                            <td><label class="control-label">Date of purchase</label></td>
                            <td><?php echo $all_item_by_id->date_of_purchase ?></td>
                        </tr>
                        <tr class="">
                            <td><label for="doj" class="control-label">Purchase from</label></td>
                            <td><?php echo $all_item_by_id->purchase_from ?></td>
                        </tr>
                        <tr class="">
                            <td><label for="ex" class="control-label">Reference</label></td>
                            <td>
                                <?php echo $all_item_by_id->reference ?>
                            </td>
                        </tr>
                        <tr class="">
                            <td><label for="qf" class="control-label">Supplier</label></td>
                            <td><?php echo $all_item_by_id->supplier ?></td>
                        </tr>
                        <tr class="">
                            <td><label for="dg" class="control-label">Contact number</label></td>
                            <td><?php echo $all_item_by_id->contact_number ?></td>
                        </tr>
                        <tr class="">
                            <td><label for="dg" class="control-label">Email</label></td>
                            <td> <?php echo $all_item_by_id->email ?></td>
                        </tr>
                        <tr class="">
                            <td colspan="2">   
                                <a href="<?php echo base_url() ?>admin/print_item/<?php echo $all_item_by_id->item_no; ?>" target="_"class="btn btn-info btn-block">Print</a>
                                <a href="<?php echo base_url() ?>admin/create_invoice/<?php echo $all_item_by_id->item_no; ?>" class="btn btn-success btn-block">Save As PDF</a>
                            
                            </td>
                        </tr>
                    </table>
                    </fieldset>

                </div>
            </div>
        </div>
    </div>
</div>
