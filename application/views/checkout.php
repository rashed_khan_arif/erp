<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <a href="<?php echo base_url() ?>admin/view_invoice" class="btn btn-info pull-left" >View Invoice</a>       
            <br/>
            <br/>
        </div>
        <div class="panel-body">
            <?php
            $msg = $this->session->userdata('msg');
            if ($msg) {
                echo "<script>alert('$msg')</script>";
                $this->session->unset_userdata('msg');
            }
            ?>
            <div class="row">
                <div class="col-md-12 container text-center">
                    <h3>
                        Create An Order
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 container">
                    <form action="<?php echo base_url() ?>checkout/select_customer" method="post">
                        <div class="col-md-4 text-center">
                            <label><p class="text-center">Customer ID:</p></label>
                        </div>
                        <div class="col-md-4">
                            <div class="control-group">
                                <input type="text" class="form-control" id="exampleInputAmount" required name="c_id">
                            </div>
                            <div class="control-group">
                                <input type="submit" class="btn btn-info btn-sm pull-right" value="Enter">
                            </div>
                        </div>
                        <div class="col-md-4"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>