<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <a href="<?php echo base_url() ?>admin/add_product" class="btn btn-info pull-left">Add New Product</a>         
            <form class="form-inline" action="<?php echo base_url() ?>admin/search_product" method="get">
                <input type="submit" class="btn btn-info pull-right" value="search">
                <input type="text" class="form-control pull-right" required name="search" placeholder="type ID or name">         
            </form><br/><br/>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-responsive table-bordered table-hover table-striped text-center tbl_color">
                                <thead>
                                    <tr class="success">
                                        <th class="text-center">Product ID</th>
                                        <th class="text-center">Product Name</th> 
                                        <th class="text-center">Quantity</th>
                                        <th class="text-center">Price Per Quantity</th>       
                                        <th class="text-center">Discount</th>       

                                        <th class="text-center">Vat</th>                           
                                        <th class="text-center">Price After Vat</th>       
                                        <th class="text-center">Total Price</th>                      
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php foreach ($select_product as $v_em) { ?>
                                        <tr>
                                            <td><?php echo $v_em->product_id ?></td>
                                            <td><?php echo $v_em->product_name ?></td>      
                                            <td><?php echo $v_em->product_quantity ?></td>   
                                            <td><?php echo $v_em->product_price_per_quantity ?></td>   
                                            <td><?php echo $v_em->discount ?>&nbsp;&nbsp;%</td>                 
                                            <td><?php echo $v_em->vat ?>&nbsp;&nbsp;%</td>   
                                            <td><?php echo $v_em->price_after_vat ?></td>   
                                            <td><?php echo $v_em->price ?></td>   

                                            <td>
                                                <script type="text/javascript">
                                                    function check() {
                                                        var chk = confirm('Are You sure ??');
                                                        if (chk) {
                                                            return true;
                                                        }
                                                        eles{
                                                            return false;
                                                        }
                                                    }
                                                </script>
                                                <a class="btn btn-info btn-sm" title="Click To View" href="<?php echo base_url() ?>admin/view_product/<?php echo $v_em->p_id ?>"> <li class="glyphicon glyphicon-eye-open">View</li></a>
                                                <a class="btn btn-primary btn-sm" title="Click To Edit" href="<?php echo base_url() ?>admin/edit_product/<?php echo $v_em->p_id ?>"> <li class="glyphicon glyphicon-edit">Edit</li></a>
                                                <a class="btn btn-danger btn-sm" title="Click To Delete" href="<?php echo base_url() ?>admin/delete_product/<?php echo $v_em->p_id ?>"> <li class="glyphicon glyphicon-trash">Del</li></a>
                                            </td>
                                        </tr>
                                    <?php } ?>  

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>