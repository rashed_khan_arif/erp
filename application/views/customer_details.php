<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <a href="<?php echo base_url() ?>admin/view_customer" class="btn btn-info">View Customer</a>
            <a href="<?php echo base_url() ?>admin/add_customer" class="btn btn-info">Add Customer</a>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-8 col-md-offset-2" style="border: solid 1px #ccc;">
                    <div class="container centered">
                    </div><br/>
                    <fieldset>
                        <?php
                        $msg = $this->session->userdata('msg');
                        if ($msg) {
                            echo "<script>alert('$msg')</script>";
                            $this->session->unset_userdata('msg');
                        }
                        ?>
                        <table class="table table-condensed table-hover table-striped table-bordered">
                            <tr class="success msg">
                                <td colspan="2" class="text-center"><span style="font-size: 30px">Customer Details</span></td>
                            </tr>

                            <tr class="">
                                <td><label for="fname" class="control-label">customer ID</label></td>
                            <input type="hidden" class="box" name="item_no" required id="fname">
                            <td><?php echo $select_customer->customer_id ?></td>
                            </tr>
                            <tr class="">
                                <td><label for="lname" class="control-label">Organization name</label></td>
                                <td><?php echo $select_customer->organization_name ?></td>
                            </tr>
                            <tr class="">
                                <td><label for="bank" class="control-label">Reference</label></td>
                                <td><?php echo $select_customer->reference ?></td>
                            </tr>
                            <tr class="">
                                <td><label for="dob" class="control-label">Date of Join</label></td>
                                <td>
                                    <?php echo $select_customer->date_of_join ?>
                                </td>
                            </tr>
                            <tr class="">
                                <td><label for="id" class="control-label">Address</label></td>
                                <td><?php echo $select_customer->address ?></td>
                            </tr>

                            <tr class="">
                                <td><label for="em" class="control-label">phone</label></td>
                                <td><?php echo $select_customer->phone ?></td>
                            </tr>
                            <tr class="">
                                <td><label for="email" class="control-label">Email</label></td>
                                <td><?php echo $select_customer->email ?></td>
                            </tr>
                            <tr class="">
                                <td><label class="control-label">Credit</label></td>
                                <td><?php echo $select_customer->credit ?></td>
                            </tr>         
                            <tr class="">
                                <td><label class="control-label">Credit Amount</label></td>
                                <td><?php echo $select_customer->credit_amount ?></td>
                            </tr>
                            <tr class="">
                                <td><label for="doj" class="control-label">Shipment Address</label></td>
                                <td><?php echo $select_customer->shipment_address ?></td>
                            </tr>   
                            <tr class="">
                                <td colspan="2">   
                                    <a href="<?php echo base_url() ?>admin/print_customer/<?php echo $select_customer->c_id; ?>" target="_"class="btn btn-info btn-block">Print</a>
                                    <a href="<?php echo base_url() ?>admin/create_cust_invoice/<?php echo $select_customer->c_id; ?>" class="btn btn-primary btn-block">Save As PDF</a>
                                </td>
                            </tr>
                        </table>
                    </fieldset>

                </div>
            </div>
        </div>
    </div>
</div>
