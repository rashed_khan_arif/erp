<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Enterprise Resource Planning</title>  
        <link href="<?php echo base_url(); ?>asset/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>asset/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>asset/css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>   
        <link href="<?php echo base_url(); ?>asset/css/style.css" rel="stylesheet" type="text/css"/>
        <script src="<?php echo base_url() ?>asset/js/jquery-1.4.2.min.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" media="screen" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/base/jquery-ui.css">
        <link class="jsbin" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
        <script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
        <script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.0/jquery-ui.min.js"></script>

        <style type="text/css">
            .active { background-color: green; }
        </style>
    </head>

    <body class="bk">
        <br/>
        <div class="container" id="printarea">
            <div class="panel panel-default">
                <div class="panel-heading">   
                    <a href="<?php echo base_url() ?>admin/invoice/<?php echo $order_info->order_id ?>" class="pull-left"> <span class="glyphicon glyphicon-save"></span></a><a onclick="printDi('printarea')" class="pull-right" style="cursor:pointer"><span class="glyphicon glyphicon-print"></span></a>&nbsp;&nbsp;&nbsp;&nbsp;
                </div>
                <div class="panel-body bkbl">
                    <div class="text-center">

                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-12 bg-success">
                                Invoice View
                            </div>
                            <table class="table table-responsive table-bordered">
                                <tr>
                                    <td>
                                        <mark><strong>Invoice TO</strong></mark> <br/>
                                        <strong> Customer ID </strong># <br/>
                                        <strong> Customer Name</strong> #<br/>
                                        <strong>Customer Address</strong> #<br/>
                                        <strong> Customer Email</strong> #<br/>
                                        <strong> Customer Phone</strong> # <br/>
                                    </td>
                                    <td><br/>
                                        <?php echo $order_info->customer_id ?><br/>
                                        <?php echo $order_info->organization_name ?><br/>
                                        <?php echo $order_info->address ?><br/>
                                        <?php echo $order_info->email ?><br/>
                                        <?php echo $order_info->phone ?><br/>  
                                    </td>
                                    <td>
                                        <strong><strong>Invoice No :INV</strong>#<?php echo $order_info->order_id ?></strong><br/>  <br/>
                                        <strong>Reccived By</strong> &nbsp;&nbsp;:<?php echo $order_info->first_name ?>&nbsp;<?php echo $order_info->last_name ?><br/>  <br/>
                                        <strong>Invoice Date</strong>&nbsp;&nbsp; :<?php echo $order_info->order_date_time ?><br/>  
                                    </td>
                                </tr>
                            </table>
                        </div>  
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-12 bg-success">
                                Order Details
                            </div>
                            <table class="table table-responsive table-bordered">
                                <tr>
                                    <th>Product ID</th>
                                    <th>Product Name</th>
                                    <th>Unit Price</th>
                                    <th>Discount</th>
                                    <th>Vat</th>
                                    <th>Price Discount +Vat</th>
                                    <th>Quantity</th>
                                    <th>Sub Total</th>
                                </tr>
                                <?php
                                $total = 0;
                                foreach ($order_full_details as $v_p) {
                                    ?>
                                    <tr>
                                        <td><?php echo $v_p->product_id ?></td>  
                                        <td><?php echo $v_p->product_name ?></td>  
                                        <td><?php echo $v_p->product_price_per_quantity ?></td>  
                                        <td><?php echo $v_p->discount ?></td>  
                                        <td><?php echo $v_p->vat ?></td>  
                                        <td><?php echo $v_p->price_after_vat ?></td>  
                                        <td><?php echo $v_p->product_sales_quantity ?></td>   
                                        <td><?php
                                            echo $v_p->price_after_vat * $v_p->product_sales_quantity;
                                            ?>
                                        </td>  
                                    </tr>
                                    <?php $total = $total + $v_p->price_after_vat * $v_p->product_sales_quantity; ?>
                                <?php } ?>
                                <tr class="success">
                                    <td colspan="6"></td>
                                    <td class="info">Order Total :</td>
                                    <td class="info">
                                        <?php
                                        echo $total;
                                        ?>
                                    </td>
                                </tr>
                                <tr class="success">
                                    <td colspan="6"></td>
                                    <td>Order Discount :</td>
                                    <td>
                                        <?php
                                        $dis = ($order_info->discount * $total) / 100;
                                        echo $dis;
                                        ?>
                                    </td>
                                </tr>
                                <tr class="success">
                                    <td colspan="6"></td>
                                    <td>Order Vat:</td>
                                    <td>
                                        <?php
                                        $vat = ($order_info->vat * $total) / 100;
                                        echo $vat;
                                        ?>
                                    </td>
                                </tr>
                                <tr class="success">
                                    <td colspan="6"></td>
                                    <td class="info">Grand Total :</td>
                                    <td class="info">
                                        <?php
                                        $dis = ($order_info->discount * $total) / 100;
                                        $vat = ($order_info->vat * $total) / 100;
                                        $grandtotal = ($total - $dis) + $vat;
                                        echo $grandtotal;
                                        ?>
                                    </td>
                                </tr>
                                <tr class="success">
                                    <td colspan="6"></td>
                                    <td><mark>Payment Type</mark>:</td>
                                    <td>
                                        <mark> <?php
                                            echo $order_info->payment_type;
                                            ?></mark>
                                    </td>
                                </tr>
                                <tr class="success">
                                    <td colspan="6"></td>
                                    <td>Pay Amount :</td>
                                    <td>
                                        <?php
                                        echo $order_info->cash;
                                        ?>
                                    </td>
                                </tr>
                                <tr class="success">
                                    <td colspan="6"></td>
                                    <td>Due Amount :</td>
                                    <td>
                                        <?php
                                        $due = $grandtotal - $order_info->cash;
                                        if ($due <= 0) {
                                            echo "Paid";
                                        } else {
                                            if ($order_info->payment_type == "bank") {
                                                echo "Paid";
                                            } else {
                                                echo $due;
                                            }
                                        }
                                        ?>
                                    </td>
                                </tr>
                            </table>
                        </div>  
                    </div>
                    <script type="text/javascript">
                        function printDi(printarea) {
                            var printContents = document.getElementById(printarea).innerHTML;
                            var originalContents = document.body.innerHTML;

                            document.body.innerHTML = printContents;

                            window.print();

                            document.body.innerHTML = originalContents;
                        }
                    </script>
                    <div class="col-md-12 bg">
                        <div class="text-center cl"><span class="pull-left">&copy;&nbsp;Copyright rashedkhan.com </span><span class="text-center">2015</span><span class="pull-right">Design & Development Rashed Khan arif</span> </div>
                    </div>
                </div>

            </div>
        </div>

    </body>
</html>