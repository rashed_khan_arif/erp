<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <a href="<?php echo base_url() ?>admin/salary_payment" class="btn btn-info">Make Payment</a>
            <a href="<?php echo base_url() ?>admin/salary_statement" class="btn btn-info">Salary Statement</a>
            <a href="<?php echo base_url() ?>admin/salary" class="btn btn-info">View Salary</a>
            <a href="<?php echo base_url() ?>admin/view_pay_salary" class="btn btn-info">View Pay Salary</a>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <form action="<?php echo base_url(); ?>admin/save_salary_payment" method="post">
                        <table class="table table-responsive table-hover table-striped tbl_color">
                            <tr class="info text-center">
                                <td colspan="4"><span style="font-size: 22px; font-weight: bold;">Pay Employee Salary</span> </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>Employee ID</td>
                            <input type="hidden" name="si_id" value="<?php echo $select_employee->si_id ?>">
                            <td><input type="hidden" name="emp_id" value="<?php echo $select_employee->emp_id ?>"><?php echo $select_employee->employee_id ?></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>Salary Month And year:</td>
                                <td>
                                    <input type="text"  readonly name="month" value="<?php echo $select_employee->month ?>">
                                    <input type="text" name="year" readonly value="<?php echo $select_employee->year ?>">
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>Pay By:</td>
                                <td>
                                    <input type="radio" value="cash" name="payment_type" onclick="on()" id="cashs">&nbsp;Cash
                                    <input type="radio" value="bank" name="payment_type" onclick="on2()" id="banks">&nbsp;Bank
                                </td>
                            </tr>
                            <script type="text/javascript">
                                function on() {
                                    document.getElementById("cash").style.display = '';
                                    document.getElementById("bank").style.display = 'none';
                                }
                                function on2() {
                                    document.getElementById("cash").style.display = 'none';
                                    document.getElementById("bank").style.display = '';
                                }
                            </script>

                            <tr style="display: none" id="cash">
                                <td></td>
                                <td></td>
                                <td><input type="text" name="cash" placeholder="Type Amount"></td>
                            <input type="hidden" name="si_id" value="<?php echo $select_employee->si_id ?>" placeholder="Type Amount">
                            </tr>
                            <tr style="display: none" id="bank">
                                <td></td>
                                <td></td>
                                <td><input type="text" name="bank" placeholder="Type bank Ac. no"></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>Note:</td>
                                <td>
                                    <textarea name="note" class="form-control"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>Payment Date:</td>
                                <td>
                                    <input type="date" class="form-control" name="payment_date" placeholder="yy-mm-dd">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <input type="submit" value="Save" class="btn btn-block btn-info">
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
