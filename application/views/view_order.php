<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <a href="<?php echo base_url() ?>admin/view_invoice" class="btn btn-info pull-left" >View Invoice</a>       
            <form class="form-inline" action="<?php echo base_url() ?>admin/search_product_for_invoice" method="get">
                <input type="submit" class="btn btn-info pull-right" value="Search Product">
                <input type="text" class="form-control pull-right" required name="search" placeholder="Search By Product ID">
            </form>
            <br/><br/>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered table-hover table-striped text-center tbl_color">
                                <thead>
                                    <tr class="success">
                                        <th class="text-center">Product ID</th>
                                        <th class="text-center">Product Name</th> 
                                        <th class="text-center">Stoke In</th>
                                        <th class="text-center">Price Per Quantity</th>       
                                        <th class="text-center">Discount</th>     
                                        <th class="text-center">Vat</th>                           
                                        <th class="text-center">Price<br/>(Including Vat After Discount)</th>                            
                                        <th class="text-center">Quantity</th>            
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php foreach ($select_product as $v_em) { ?>
                                        <tr>
                                    <form action="<?php echo base_url() ?>cart/add_to_cart/<?php echo $v_em->p_id ?>" method="post">
                                        <td><?php echo $v_em->product_id ?></td>
                                        <td><?php echo $v_em->product_name ?></td>               
                                        <td><?php echo $v_em->product_quantity ?></td>   
                                        <td><?php echo $v_em->product_price_per_quantity ?></td>   
                                        <td><?php echo $v_em->discount ?>&nbsp;&nbsp;%</td>   
                                        <td><?php echo $v_em->vat ?>&nbsp;&nbsp;%</td>         
                                        <td><?php echo $v_em->price_after_vat ?></td>   
                                        <td><input class="boxex" type="number" value="1" name="qty"></td>   
                                        <td>
                                            <button class="btn btn-info btn-sm" type="submit">Add To Invoice</button>    
                                        </td>
                                        </tr>
                                    </form>
                                <?php } ?>  

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>