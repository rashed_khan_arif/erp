<html>
    <head>
        <link href="<?php echo base_url(); ?>asset/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>asset/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>asset/css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>   
        <link href="<?php echo base_url(); ?>asset/css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="panel panel-default" id="printarea">
            <div class="panel-body bk">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <fieldset>
                            <table class="table table-condensed table-hover table-striped table-bordered">
                               
                                <tr class="info msg">
                                    <td colspan="2" class="text-center"><span style="font-size: 30px">Customer Details</span></td>
                                </tr>

                                <tr class="info">
                                    <td><label for="fname" class="control-label">customer ID</label></td>
                                <input type="hidden" class="box" name="item_no" required id="fname">
                                <td>
                                    <?php echo $select_customer->customer_id ?>
                                </td>
                                </tr>
                                <tr class="info">
                                    <td><label for="lname" class="control-label">Organization name</label></td>
                                    <td>
                                        <?php echo $select_customer->organization_name ?>
                                    </td>
                                </tr>
                                <tr class="info">
                                    <td><label for="bank" class="control-label">Reference</label></td>
                                    <td>
                                        <?php echo $select_customer->reference ?>
                                    </td>
                                </tr>
                                <tr class="info">
                                    <td><label for="dob" class="control-label">Date of Join</label></td>
                                    <td>
                                        <?php echo $select_customer->date_of_join ?>
                                    </td>
                                </tr>
                                <tr class="info">
                                    <td><label for="id" class="control-label">Address</label></td>
                                    <td><?php echo $select_customer->address ?></td>
                                </tr>

                                <tr class="info">
                                    <td><label for="em" class="control-label">phone</label></td>
                                    <td><?php echo $select_customer->phone ?></td>
                                </tr>
                                <tr class="info">
                                    <td><label for="email" class="control-label">Email</label></td>
                                    <td><?php echo $select_customer->email ?></td>
                                </tr>
                                <tr class="info">
                                    <td><label class="control-label">Credit</label></td>
                                    <td><?php echo $select_customer->credit ?></td>
                                </tr>         
                                <tr class="info">
                                    <td><label class="control-label">Credit Amount</label></td>
                                    <td><?php echo $select_customer->credit_amount ?></td>
                                </tr>
                                <tr class="info">
                                    <td><label for="doj" class="control-label">Shipment Address</label></td>
                                    <td><?php echo $select_customer->shipment_address ?></td>
                                </tr>   

                                 <script type="text/javascript">
                                    function printDi(printarea) {
                                        var printContents = document.getElementById(printarea).innerHTML;
                                        var originalContents = document.body.innerHTML;

                                        document.body.innerHTML = printContents;

                                        window.print();

                                        document.body.innerHTML = originalContents;
                                    }
                                </script>
                                <tr class="info">
                                    <td colspan="2">   
                                        <a href="" class="btn btn-info btn-block" onclick="printDi('printarea')"  id="print">Print</a>
                                    </td>
                                </tr>
                            </table>

                        </fieldset>

                    </div>
                </div>
            </div>
        </div>
    </body>
</html>