<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title>AdminLTE | Log in</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="<?php echo base_url() ?>asset/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>asset/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>asset/css/AdminLTE.css" rel="stylesheet" type="text/css" />
    </head>
    <body class="bg-black">
        <div class="form-box" id="login-box">
            <div class="header">Sign In</div>
            <form action="<?php echo base_url() ?>welcome/check_login" method="post">

                <div class="body bg-gray">
                    <?php $msg = $this->session->userdata('message');
                    if ($msg) {
                        ?>
                        <p style="color: red">
                            <?php echo $msg;
                            $this->session->unset_userdata('message');
                            ?>
                        </p>
<?php } ?>
                    <div class="form-group">
                        <input type="email" name="admin_email" class="form-control" placeholder="User Email"/>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Password"/>
                    </div>          
                </div>
                <div class="footer">                                                               
                    <button type="submit" class="btn bg-olive btn-block">Sign me in</button>              
                    <p><a href="#">I forgot my password</a></p>

                </div>
            </form>
        </div>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="<?php echo base_url() ?>asset/js/bootstrap.min.js" type="text/javascript"></script>        

    </body>
</html>