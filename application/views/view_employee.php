<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <form class="form-inline" action="<?php echo base_url() ?>admin/search_employee" method="get">
                <input type="submit" class="btn btn-info pull-right" value="search">
                <input type="text" class="form-control pull-right" name="search" placeholder="type ID or name and Enter">
            </form>
            <a href="<?php echo base_url() ?>admin/add_employee" class="btn btn-info left">Add Employee</a>
            <a href="<?php echo base_url() ?>admin/inactive_employee" class="btn btn-info pull-left">Inactive Employee</a>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-responsive table-bordered table-hover table-striped text-center tbl_color">
                        <thead>
                            <tr class="success">
                                <th class="text-center">ID</th>
                                <th class="text-center">Name<!--first+lastname--></th>
                                <th class="text-center">Designation</th>
                                <th class="text-center">Joining Date</th>
                                <th class="text-center">Qualification</th>
                                <th class="text-center">Experience</th>
                                <th class="text-center">Contact NO</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Action</th>
                            </tr>

                        </thead>
                        <tbody>
                            <?php foreach ($view_em as $v_em) { ?>
                                <tr>
                                    <td><?php echo $v_em->employee_id ?></td>
                                    <td><?php echo $v_em->first_name . $v_em->last_name ?></td>
                                    <td><?php echo $v_em->designation ?></td>
                                    <td><?php echo $v_em->doj ?></td>
                                    <td><?php echo $v_em->qualification ?></td>
                                    <td><?php echo $v_em->experience ?></td>
                                    <td><?php echo $v_em->contact ?></td>
                                    <td><?php if ($v_em->status == 1) { ?>

                                <li class="glyphicon glyphicon-user"><br/>Active</li><br/>
                            <?php } else {
                                ?>
                                <li class = "glyphicon glyphicon-ban-circle">&nbsp;<br/>
                                    Inactive</li>
                            <?php } ?>

                            </td>
                            <td>
                                <?php if ($v_em->status == 0) { ?>
                                    <a class="btn btn-info btn-sm" title="Click To Active" href="<?php echo base_url() ?>admin/published_employee/<?php echo $v_em->emp_id ?>"> <li class="glyphicon glyphicon-ok"></li></a>
                                <?php } else { ?>
                                    <a class="btn btn-info btn-sm" title="Click To InActive" href="<?php echo base_url() ?>admin/un_published_employee/<?php echo $v_em->emp_id ?>"> <li class="glyphicon glyphicon-unchecked"></li></a>
                                <?php } ?>
                                <script type="text/javascript">
                                    function check() {
                                        var chk = confirm('Are You sure ??');
                                        if (chk) {
                                            return true;
                                        }
                                        eles{
                                            return false;
                                        }
                                    }
                                </script>
                                <a class="btn btn-default btn-sm" title="Click To View" href="<?php echo base_url() ?>admin/view_en_employee/<?php echo $v_em->emp_id ?>"> <li class="glyphicon glyphicon-eye-open"></li></a>
                                <a class="btn btn-primary btn-sm" title="Click To Edit" href="<?php echo base_url() ?>admin/edit_employee/<?php echo $v_em->emp_id ?>"> <li class="glyphicon glyphicon-edit"></li></a>
                                <a class="btn btn-danger btn-sm" title="Click To Delete" onclick="return check();" href="<?php echo base_url() ?>admin/delete_employee/<?php echo $v_em->emp_id ?>"> <li class="glyphicon glyphicon-trash"></li></a>

                            </td>
                            </tr>
                        <?php } ?>  

                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>