<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <a href="<?php echo base_url() ?>admin/unpaid_orders" class="btn btn-info pull-left" >All Unpaid Invoice</a>             

            <?php if ($this->session->userdata('c_id')) { ?>
                <a href="<?php echo base_url() ?>cart/show_cart" class="btn btn-info pull-left" >Complete Last order</a>             
                <a href="<?php echo base_url() ?>admin/logout_customer" class="btn btn-info pull-left" >Cancel Last order</a>             
            <?php } ?>       
            <form class="form-inline" action="<?php echo base_url() ?>admin/search_order" method="get">
                <input type="submit" class="btn btn-info pull-right" value="Search Invoice">
                <input type="text" class="form-control pull-right" required name="search" placeholder="Search By Invoice ID">
            </form>
            <br/><br/>
        </div>
        <?php
        $msg = $this->session->userdata('msg');
        if ($msg) {
            echo "<script>alert('$msg')</script>";
            $this->session->unset_userdata('msg');
        }
        ?>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">


                    <table class="table table-bordered table-hover table-striped text-center tbl_color">
                        <thead>
                            <tr class="success">
                                <th class="text-center">Invoice ID</th>   
                                <th class="text-center">Customer ID</th>  
                                <th class="text-center">Customer Name</th>  
                                <th class="text-center">Order Total</th>     
                                <th class="text-center">Order Date Time</th>     
                                <th class="text-center">Action</th>            
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($select_invoice as $v_em) {
                                ?>
                                <tr>
                                    <td><?php echo $v_em->order_id ?></td>                
                                    <td><?php echo $v_em->customer_id ?></td>   
                                    <td><?php echo $v_em->organization_name ?></td>   
                                    <td><?php echo $v_em->order_total ?></td>   
                                    <td><?php echo $v_em->order_date_time ?></td>   
                                    <td>
                                        <a href="<?php echo base_url() ?>admin/view_invoice_details/<?php echo $v_em->order_id ?>" target="_" class="btn btn-info btn-sm"><li class="glyphicon glyphicon-eye-open"></li></a>
                                        <?php if ($v_em->cash == null && $v_em->payment_type == "bank") { ?>
                                            <a class="btn btn-success btn-sm"><li class="glyphicon">Paid</li></a>
                                        <?php } else { ?>
                                            <?php
                                            $cash = $v_em->cash;
                                            $total = $v_em->order_total;
                                            $result = $total - $cash;
                                            // echo $result;
                                            ?>
                                            <?php if ($result <= 0) { ?>
                                                <a class="btn btn-success btn-sm"><li class="glyphicon">Paid</li></a>
                                            <?php } else { ?>
                                                <a href="<?php echo base_url() ?>admin/paid_order/<?php echo $v_em->order_id ?>" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Click to paid this"><li class="glyphicon">Unpaid</li></a>

                                            <?php } ?>
                                        <?php } ?>
                                    </td>                         
                                </tr>
                            <?php } ?>  
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>