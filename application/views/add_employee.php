
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <a href="<?php echo base_url() ?>admin/view_employee" class="btn btn-info">View Active Employee</a>
            <a href="<?php echo base_url() ?>admin/inactive_employee" class="btn btn-info">Inactive Employee</a>  
        </div>
         <?php
        $msg = $this->session->userdata('msg');
        if ($msg) {
            echo "<script>alert('$msg')</script>";
            $this->session->unset_userdata('msg');
        }
        ?>
        <div class="panel-body">
            <div class="row">
                <form action="<?php echo base_url() ?>admin/save_employee" method="post" enctype="multipart/form-data">  
                    <div class="col-md-12">
                        <div class="col-md-5 panel panel-default">  
                            <div class="panel-heading">
                               Personal Details
                            </div>
                            <table class="table table-bordered table-striped">
                                <tr class="">
                                    <td><label for="id" class="control-label">Employee ID</label></td>
                                    <td><input type="text" class="box" autofocus name="employee_id" maxlength="20" required id="id"></td>
                                </tr>
                                <tr class="">
                                    <td><label for="fname" class="control-label">Employee First Name</label></td>
                                    <td><input type="text" class="box" placeholder="Ex :Jhone" name="first_name" required id="fname"></td>
                                </tr>
                                <tr class="">
                                    <td><label for="lname" class="control-label">Employee last Name</label></td>
                                    <td><input type="text" class="box" placeholder="Ex :doe" name="last_name" required id="lname"></td>
                                </tr>
                                <tr class="">
                                    <td><label for="bank" class="control-label">Bank Account No</label></td>
                                    <td><input type="text" class="box" required name="bank" id="bank"></td>
                                </tr>
                                <tr class="">
                                    <td><label for="bank" class="control-label">Blood Group</label></td>
                                    <td>
                                        <select class="box" name="blood_group">                                 
                                            <option>Select blood group</option>
                                            <option value="A+">A+</option>
                                            <option value="A-">A-</option>
                                            <option value="B+">B+</option>
                                            <option value="B-">B-</option>
                                            <option value="O+">O+</option>
                                            <option value="O-">O-</option>
                                            <option value="AB+">AB+</option>
                                            <option value="AB-">AB-</option>
                                        </select>

                                    </td>
                                </tr>
                                <tr class="">
                                    <td><label for="bank" class="control-label">Gender</label></td>  
                                    <td>
                                        <input type="radio" name="gender" class="control-label" value="Male">&nbsp;  Male&nbsp;
                                        <input type="radio" name="gender" value="Female">&nbsp; Female
                                    </td>
                                </tr>
                                <tr class="">
                                    <td><label for="dob" class="control-label">Date of Birth</label></td>
                                    <td><input type="date" class="box" placeholder="Ex. month-day-year" required name="dob" id="dob"></td>
                                </tr>
                                <tr class="">
                                    <td><label for="lname" class="control-label">Employee Image</label></td>
                                    <td>
                                        <input type="file" onchange="readURL(this);" class="fileUpload uniform_on" name="image">

                                    </td>
                                </tr>
                            </table>
                        </div>
                   
                        <div class="col-md-5 col-md-offset-2 panel panel-default">
                            <div class="panel-heading">
                               Contact Details
                            </div>
                            <table class="table table-bordered table-striped">

                                <script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
                                <script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.0/jquery-ui.min.js"></script>

                                <script>
                                            function readURL(input) {
                                                if (input.files && input.files[0]) {
                                                    var reader = new FileReader();

                                                    reader.onload = function (e) {
                                                        $('#image')
                                                                .attr('src', e.target.result)

                                                                .width(150)
                                                                .height(150);
                                                    };

                                                    reader.readAsDataURL(input.files[0]);
                                                }
                                            }
                                </script>
                                <tr class="">
                                    <td><label class="control-label">Present Address</label></td>
                                    <td>
                                        <textarea name="present_address" class="area"></textarea>
                                    </td>
                                </tr>
                                <tr class="">
                                    <td><label class="control-label">Permanent Address</label></td>
                                    <td>
                                        <textarea name="permenent_address" class="area"></textarea>
                                    </td>
                                </tr>
                                <tr class="">
                                    <td><label for="id" class="control-label">Identification</label></td>
                                    <td><input type="text" class="box" required name="identification" id="id"></td>
                                </tr>
                                <tr class="">
                                    <td><label for="con" class="control-label">Contact No</label></td>
                                    <td>
                                        <input type="number" class="box" maxlength="15" required name="contact" id="con">
                                    </td>
                                </tr>
                                <tr class="">
                                    <td><label for="em" class="control-label">Emergency Contact No</label></td>
                                    <td><input type="text" class="box" required name="em_contact" id="em"></td>
                                </tr>
                                <tr class="">
                                    <td><label for="email" class="control-label">Email No</label></td>
                                    <td><input type="email" class="box" required name="email" id="email"></td>
                                </tr>


                            </table>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6 col-md-offset-3 panel panel-default">
                            <div class="panel-heading">
                               Job Details
                            </div>
                            <table class="table table-bordered table-striped">
                                <tr class="">
                                    <td><label class="control-label">Start Date</label></td>
                                    <td><input type="date" class="box" placeholder="Ex. month-day-year" id="datepicker" name="start_date" required ></td>
                                </tr>         
                                <tr class="">
                                    <td><label class="control-label">End Date</label></td>
                                    <td><input type="date" class="box" placeholder="Ex. month-day-year" class="datepicker" name="end_date" required ></td>
                                </tr>
                                <tr class="">
                                    <td><label for="doj" class="control-label">Date of Join</label></td>
                                    <td><input type="date" class="box" placeholder="Ex. month-day-year" required name="doj" id="doj"></td>
                                </tr>
                                <tr class="">
                                    <td><label for="ex" class="control-label">Experience</label></td>
                                    <td>
                                        <input type="text" placeholder="Total Experience" class="boxex" maxlength="2" required name="experience" id="ex">
                                        <select class="boxex" name="my">
                                            <option>Select month/year</option>
                                            <option value="Year">Year</option>
                                            <option value="Month">Month</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr class="">
                                    <td><label for="qf" class="control-label">Qualification</label></td>
                                    <td><input type="text" placeholder="Educational Qualification" class="box" required name="qualification" id="qf"></td>
                                </tr>
                                <tr class="">
                                    <td><label for="name" class="control-label">Job Status</label></td>
                                    <td>
                                        <select class="box" name="job_status" onchange="CheckColors(this.value)">                                 
                                            <option>Select Job status</option>
                                            <option value="Part Time">Part Time</option>
                                            <option value="Full Time">Full Time</option>
                                            <option value="Contractual">Contractual</option>
                                        </select>
                                    </td>
                                </tr>
                                <script type="text/javascript">
                                    function CheckColors(val) {
                                        var element = document.getElementById('res');
                                        if (val == 'Part Time' || val == 'Contractual')
                                            element.style.display = 'block';
                                        else
                                            element.style.display = 'none';
                                    }

                                </script>

                                <tr class="">
                                    <td></td>
                                    <td><input type="text" id="res" class="box" placeholder="Enter Schedule *ex: 3 days per week" style="display: none" name="job_status_info"></td>
                                </tr>
                                <tr class="">
                                    <td><label for="dg" class="control-label">Designation</label></td>
                                    <td><input type="text" placeholder="Ex: Web Developer" class="box" required name="designation" id="dg"></td>
                                </tr>
                                <tr class="">
                                    <td><label class="control-label">Status</label></td>
                                    <td><input type="radio" name="status" class="control-label" value="1">&nbsp;Active&nbsp;
                                        <input type="radio" name="status" value="0">&nbsp;Inactive
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <table class="table table-responsive">
                            <tr class="text-center">
                                <td><label class="control-label"></label></td>
                                <td><input type="checkbox" required name="agree">&nbsp; &nbsp;All Information is correct</td>
                            </tr>
                            <tr class="">
                                <td colspan="2">
                                    <button type="submit" class="btn btn-info btn-block">Save Employee Information</button>
                                    <button type="reset" class="btn btn-block btn-default">Cancel</button>
                                </td>
                            </tr>
                        </table>
                    </div>
                </form>
    <!--                <img src="" id="image" class="thumbnail">-->
            </div>
        </div>
    </div>
</div>
