<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            Payment
        </div>
        <div class="panel-body"> 
            <div class="row">
                <form action="<?php echo base_url() ?>checkout/confirm_order" method="post">
                    <div class="col-md-12 container"><br/>
                        <div class="col-md-4 col-md-offset-4 centered">
                            <table class="table table-bordered table-condensed table-striped">
                                <tr class="success">
                                    <td>Payment Amount:</td>
                                    <td colspan="2" class="text-center">
                                        <?php echo $this->session->userdata('g_t'); ?>
                                    </td>                 
                                </tr>
                                <tr>
                                    <td>Payment Type:<?php //echo $this->session->userdata('c_id');       ?></td>
                                    <td>
                                        <input type="radio" value="cash" onclick="on()" name="payment_type">&nbsp;&nbsp;Cash   
                                    </td>
                                    <td>
                                        <input type="radio" value="bank" onclick="on2()" name="payment_type">&nbsp;&nbsp;Bank   
                                    </td>
                                    <!--<td>
                                        <input type="radio"  onclick="both()" name="payment_type">&nbsp;&nbsp;Both   
                                    </td>-->
                                </tr>
                                <script type="text/javascript">
                                    function on() {
                                        document.getElementById("cashre").style.display = '';
                                        document.getElementById("bankre").style.display = 'none';
                                    }
                                    function on2() {
                                        document.getElementById("cashre").style.display = 'none';
                                        document.getElementById("bankre").style.display = '';
                                    }
                                    function both() {
                                        document.getElementById("cashre").style.display = '';
                                        document.getElementById("bankre").style.display = '';
                                    }
                                </script>
                                <tr id="cashre" style="display: none">
                                    <td>Pay Amount :</td>
                                    <td colspan="3">
                                        <input  placeholder="" type="text" name="cash"> 
                                    </td> 
                                </tr>
                                <tr id="">
                                    <td>Received By :</td>
                                    <td colspan="3">
                                        <select name="emp_id">
                                            <option>Select reccive employee </option>
                                            <?php foreach ($slect_active_employee as $v_ep) { ?>
                                            <option value="<?php echo $v_ep->emp_id?>"><?php echo $v_ep->employee_id?></option>
                                            <?php } ?>
                                        </select>

                                    </td> 
                                </tr>
                                <tr id="bankre" style="display: none">
                                    <td>Bank Account No. :</td>
                                    <td colspan="3">
                                        <input placeholder="bank Account" type="text" name="bank">
                                    </td> 
                                </tr>
                                <tr>
                                    <td colspan="3" class="text-center"><button type="submit" class="btn btn-info">Confirmed Order</button></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>