<html>
    <head>
        <link href="<?php echo base_url(); ?>asset/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>asset/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>asset/css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>   
        <link href="<?php echo base_url(); ?>asset/css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="panel panel-default" id="printarea">
            <div class="panel-body bk">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">

                        <fieldset>
                            <table class="table table-condensed table-hover table-striped table-bordered">

                                <tr class="success msg">
                                    <td colspan="2" class="text-center"><span style="font-size: 25px">Product Details</span></td>
                                </tr>

                                <tr class="info">
                                    <td><label for="fname" class="control-label">Product ID</label></td>
                                    <td><?php echo $select_product_id->product_id ?></td>
                                </tr>
                                <tr class="info">
                                    <td><label for="lname" class="control-label">Product Name</label></td>
                                    <td><?php echo $select_product_id->product_name ?></td>
                                </tr>
                                <tr class="info">
                                    <td><label for="bank" class="control-label">Product Category</label></td>
                                    <td>
                                        <?php echo $select_product_id->product_category ?>
                                    </td>
                                </tr>
                                <tr class="info">
                                    <td><label for="dob" class="control-label">Product Sub Category</label></td>
                                    <td>
                                        <?php echo $select_product_id->product_sub_category ?>
                                    </td>
                                </tr>
                                <tr class="info">
                                    <td><label for="id" class="control-label">Product Description</label></td>
                                    <td>   
                                       <?php echo $select_product_id->product_description ?>
                                    </td>
                                </tr>
                                <tr class="info">
                                    <td><label for="ross" class="control-label">Product Quantity</label></td>
                                    <td>
                                        <?php echo $select_product_id->product_quantity ?>  
                                    </td>
                                </tr>
                                <tr class="info">
                                    <td><label for="em" class="control-label">Product Price Per Quantity</label></td>
                                    <td>
                                        <?php echo $select_product_id->product_price_per_quantity ?>
                                    </td>
                                </tr>
                                <tr class="info">
                                    <td><label for="email" class="control-label">Discount</label></td>
                                    <td>
                                        <?php echo $select_product_id->discount ?>    &nbsp;&nbsp;%                                 
                                    </td>
                                </tr>
                                <tr class="info">
                                    <td><label for="email" class="control-label">R Discount</label></td>
                                    <td>
                                        <?php echo $select_product_id->rdiscount ?>  
                                    </td>
                                </tr>
                                <tr class="info">
                                    <td><label for="email" class="control-label">Price After Discount</label></td>
                                    <td>
                                        <?php echo $select_product_id->price_after_discount ?>  
                                    </td>
                                </tr>

                                <tr class="info">
                                    <td><label for="doj" class="control-label">Vat</label></td>
                                    <td>
                                        <?php echo $select_product_id->vat ?>  &nbsp;&nbsp;%
                                    </td>
                                </tr>
                                <tr class="info">
                                    <td><label for="doj" class="control-label">R Vat</label></td>
                                    <td>
                                        <?php echo $select_product_id->rvat ?>  
                                    </td>
                                </tr>
                                <tr class="info">
                                    <td><label for="ex" class="control-label">Price After Vat</label></td>
                                    <td>
                                        <?php echo $select_product_id->price_after_vat ?>  
                                    </td>
                                </tr>
                                <tr class="info">
                                    <td><label for="qf" class="control-label">Price</label></td>
                                    <td>
                                        <?php echo $select_product_id->price ?>
                                    </td>
                                </tr>
                                <tr class="info">
                                    <td><label for="dg" class="control-label">Storing Date</label></td>
                                    <td> 
                                        <?php echo $select_product_id->storing_date ?>
                                    </td>
                                </tr>
                                <tr class="info">
                                    <td><label for="dg" class="control-label">Storing Location</label></td>
                                    <td> 
                                        <?php echo $select_product_id->storing_location ?>
                                    </td>
                                </tr>      
                               
                                <script type="text/javascript">
                                    function printDi(printarea) {
                                        var printContents = document.getElementById(printarea).innerHTML;
                                        var originalContents = document.body.innerHTML;

                                        document.body.innerHTML = printContents;

                                        window.print();

                                        document.body.innerHTML = originalContents;
                                    }
                                </script>
                                <tr class="info">
                                    <td colspan="2">
                                        <a onclick="printDi('printarea')" class="btn btn-info btn-block">Print </a>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>