<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <a href="<?php echo base_url() ?>admin/salary" class="btn btn-info">View Salary</a>
        </div>
        <?php
        $msg = $this->session->userdata('msg');
        if ($msg) {
            echo "<script>alert('$msg')</script>";
            $this->session->unset_userdata('msg');
        }
        ?>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-8 col-md-offset-2" style="border: solid 1px #CCC">
                    <form action="<?php echo base_url() ?>admin/update_salary" name="form" method="post">
                        <table class="table table-condensed table-bordered table-hover table-striped">
                         <tr class="success text-center">
                                    <td colspan="2"><span style="font-size:20px;color: #0033DD; font-weight: bold">Salary Entry For Month</span>
                                        &nbsp;&nbsp; <select class="md" name="month">
                                            <option value="">Select Month</option>
                                            <option value="Jan">Jan</option>
                                            <option value="Feb">Feb</option>
                                            <option value="March">March</option>
                                            <option value="April">April</option>
                                            <option value="May">May</option>
                                            <option value="Jun">Jun</option>
                                            <option value="Jully">Jully</option>
                                            <option value="Aug">Aug</option>
                                            <option value="Sep">Sep</option>
                                            <option value="Oct">Oct</option>
                                            <option value="Nov">Nov</option>
                                            <option value="Dec">Dec</option>
                                        </select>&nbsp;&nbsp;&nbsp;<span style="font-size:20px;color: #0033DD; font-weight: bold">Year</span>
                                        <select class="md" name="year">
                                            <option value="">Select Year</option>
                                            <option value="2014">2014</option>
                                            <option value="2015">2015</option>
                                            <option value="2016">2016</option>
                                            <option value="2017">2017</option>
                                            <option value="2018">2018</option>
                                            <option value="2019">2019</option>
                                            <option value="2020">2020</option>
                                            <option value="2021">2021</option>
                                            <option value="2022">2022</option>
                                            <option value="2023">2023</option>
                                            <option value="2024">2024</option>
                                            <option value="2025">2025</option>
                                        </select>
                                    </td>
                                </tr>
                            <tr class="info">
                                <td><label class="control-label">Employee Name</label></td>
                                <td><input type="hidden" name="emp_id" value=" <?php echo $all_salary_by_id->emp_id ?>">
                                    <?php echo $all_salary_by_id->first_name ?>&nbsp;
                                    <?php echo $all_salary_by_id->last_name ?>
                                </td>
                            </tr>
                            <script>
                                function calculateForm() {
                                    document.getElementById("total").value = (Number(document.getElementById("basic").value)
                                            + Number(document.getElementById("increment").value)
                                            + Number(document.getElementById("house_rent").value)
                                            + Number(document.getElementById("conveyance").value)
                                            + Number(document.getElementById("medical").value)
                                            + Number(document.getElementById("gross").value)
                                            + Number(document.getElementById("overtime").value)
                                            + Number(document.getElementById("mobile").value)
                                            + Number(document.getElementById("arrear").value)
                                            + Number(document.getElementById("bonus").value)
                                            + Number(document.getElementById("others").value)
                                            + Number(document.getElementById("annual").value)
                                            + Number(document.getElementById("income_tax").value))
                                            - Number(document.getElementById("deduction").value);
                                }
                                ;

                            </script>
                            <tr class="">
                                <td><label for="fname" class="control-label">Basic_salary</label></td>
                            <input type="hidden" class="box" name="si_id" value="<?php echo $all_salary_by_id->si_id ?>">

                            <td><input type="number" class="box" onblur="calculateForm();" name="basic_salary" value="<?php echo $all_salary_by_id->basic_salary ?>" required id="basic"></td>
                            </tr>
                            <tr class="">
                                <td><label for="increment" class="control-label">Increment</label></td>
                                <td><input type="number" onblur="calculateForm();" class="box" name="increment" value="<?php echo $all_salary_by_id->increment ?>" required id="increment"></td>
                            </tr>
                            <tr class="">
                                <td><label for="house_rent" class="control-label">House_rent</label></td>
                                <td><input type="number" onblur="calculateForm();" class="box" name="house_rent" value="<?php echo $all_salary_by_id->house_rent ?>" id="house_rent"></td>
                            </tr>
                            <tr class="">
                                <td><label for="conveyance" class="control-label">Conveyance</label></td>
                                <td><input type="number" onblur="calculateForm();" class="box"  name="conveyance" value="<?php echo $all_salary_by_id->conveyance ?>" id="conveyance"></td>
                            </tr>
                            <tr class="">
                                <td><label for="medical" class="control-label">Medical</label></td>
                                <td><input type="number" onblur="calculateForm();" class="box" name="medical" value="<?php echo $all_salary_by_id->medical ?>" id="medical"></td>
                            </tr>
                            <tr class="">
                                <td><label for="gross" class="control-label">Gross</label></td>
                                <td><input type="number"  onblur="calculateForm();" class="box" name="gross" value="<?php echo $all_salary_by_id->gross ?>" id="gross"></td>
                            </tr>
                            <tr class="">
                                <td><label for="overtime" class="control-label">Overtime</label></td>
                                <td><input type="number" onblur="calculateForm();" class="box" name="overtime" value="<?php echo $all_salary_by_id->overtime ?>" id="overtime"></td>
                            </tr>
                            <tr class="">
                                <td><label for="mobile" class="control-label">Mobile</label></td>
                                <td><input type="number" onblur="calculateForm();" class="box" name="mobile" value="<?php echo $all_salary_by_id->mobile ?>" id="mobile"></td>
                            </tr>
                            <tr class="">
                                <td><label class="control-label">Arrear</label></td>
                                <td><input type="number" onblur="calculateForm();" class="box"  id="arrear" name="arrear" value="<?php echo $all_salary_by_id->arrear ?>" ></td>
                            </tr>         
                            <tr class="">
                                <td><label class="control-label">Others</label></td>
                                <td><input type="number" class="box" onblur="calculateForm();" class="datepicker" name="others" id="others" value="<?php echo $all_salary_by_id->others ?>" ></td>
                            </tr>
                            <tr class="">
                                <td><label for="bonus" class="control-label">Bonus</label></td>
                                <td><input type="number" class="box" onblur="calculateForm();"  name="bonus" id="bonus" value="<?php echo $all_salary_by_id->bonus ?>"></td>
                            </tr>
                            <tr class="">
                                <td><label for="annual" class="control-label">Annual</label></td>
                                <td>
                                    <input type="number" id="annual" onblur="calculateForm();" class="box" maxlength="2" name="annual" value="<?php echo $all_salary_by_id->annual ?>">
                                </td>
                            </tr>
                            <tr class="">
                                <td><label for="income_tax" class="control-label">Income_tax</label></td>
                                <td><input type="number" class="box" onblur="calculateForm();"  name="income_tax" value="<?php echo $all_salary_by_id->income_tax ?>" id="income_tax"></td>
                            </tr>

                            <tr class="">
                                <td><label for="deduction" class="control-label">Deduction</label></td>
                                <td><input type="number" class="box" onblur="calculateForm();" name="deduction"  value="<?php echo $all_salary_by_id->deduction ?>" id="deduction"></td>
                            </tr>
                            <tr class="">
                                <td><label for="dg" class="control-label">Total</label></td>
                                <td><input type="number" class="box"  required name="total" value="<?php echo $all_salary_by_id->total ?>" id="total"></td>
                            </tr>

                            <tr class="">
                                <td><label class="control-label"></label></td>
                                <td><input type="checkbox" required name="agree">&nbsp; &nbsp;All Information is correct</td>
                            </tr>
                            <tr class="">
                                <td colspan="2">
                                    <button type="submit" class="btn btn-info btn-block">Update Salary Information</button>
                                    <button type="reset" class="btn btn-block btn-default">Cancel</button>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    document.forms['form'].elements['emp_id'].value = '<?php echo $all_salary_by_id->emp_id ?>';
    document.forms['form'].elements['month'].value = '<?php echo $all_salary_by_id->month ?>';
    document.forms['form'].elements['year'].value = '<?php echo $all_salary_by_id->year ?>';
</script>