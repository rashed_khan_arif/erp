<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <a href="<?php echo base_url() ?>admin/view_item" class="btn btn-info">View item</a>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-8 col-md-offset-2" style="border: solid 1px #ccc">
                    <div class="container centered">
                    </div><br/>
                    <form action="<?php echo base_url() ?>admin/update_item" method="post">
                        <table class="table table-condensed table-hover table-striped table-bordered">
                            <?php
                            $msg = $this->session->userdata('msg');
                            if ($msg) {
                                ?>
                                <tr class="info msg" id="msg">
                                    <td colspan="2" class="text-center"><?php
                                        echo $msg;
                                        $this->session->unset_userdata('msg');
                                        ?></td>
                                </tr>
                            <?php } ?>
                            <tr class="info msg">
                                <td colspan="2" class="text-center"><span id="msg"></span></td>
                            </tr>
                            <tr class="success msg">
                                <td colspan="2" class="text-center"><span style="font-size: 30px">Edit Item</span></td>
                            </tr>

                            <tr class="">
                                <td><label for="fname" class="control-label">Item ID</label></td>
                            <input type="hidden" class="box" name="item_no" required id="fname">
                            <td><input type="text" class="box" value="<?php echo $all_item_by_id->item_no ?>" name="item_id" required id="fname"></td>
                            </tr>
                            <tr class="">
                                <td><label for="lname" class="control-label">Category</label></td>
                                <td><input type="text" class="box" value="<?php echo $all_item_by_id->category ?>" name="category" required id="lname"></td>
                            </tr>
                            <tr class="">
                                <td><label for="bank" class="control-label">Sub category</label></td>
                                <td><input type="text" class="box" value="<?php echo $all_item_by_id->sub_category ?>" name="sub_category" id="bank"></td>
                            </tr>
                            <tr class="">
                                <td><label for="dob" class="control-label">Description</label></td>
                                <td>
                                    <textarea name="description" class="box"><?php echo $all_item_by_id->description ?></textarea>      
                                </td>
                            </tr>
                            <tr class="">
                                <td><label for="id" class="control-label">Quantity</label></td>
                                <td><input type="number" class="box" value="<?php echo $all_item_by_id->quantity ?>" name="quantity" id="id"></td>
                            </tr>
                            <tr class="">
                                <td><label for="ross" class="control-label">Type</label></td>
                                <td><input type="text" class="box" name="type" value="<?php echo $all_item_by_id->type ?>" id="ross"></td>
                            </tr>
                            <tr class="">
                                <td><label for="em" class="control-label">Price per quantity</label></td>
                                <td><input type="text" class="box" name="price_per_quantity" value="<?php echo $all_item_by_id->price_per_quantity ?>" id="em"></td>
                            </tr>
                            <tr class="">
                                <td><label for="email" class="control-label">Invoice no</label></td>
                                <td><input type="text" class="box" name="invoice_no" value="<?php echo $all_item_by_id->invoice_no ?>" id="email"></td>
                            </tr>
                            <tr class="">
                                <td><label class="control-label">Invoice details location</label></td>
                                <td><input type="text" class="box"  id="datepicker" value="<?php echo $all_item_by_id->invoice_details_location ?>" name="invoice_details_location" ></td>
                            </tr>         
                            <tr class="">
                                <td><label class="control-label">Date of purchase</label></td>
                                <td><input type="date" placeholder="mm/dd/yyy" class="box" class="datepicker" value="<?php echo $all_item_by_id->date_of_purchase ?>" name="date_of_purchase"  ></td>
                            </tr>
                            <tr class="">
                                <td><label for="doj" class="control-label">Purchase from</label></td>
                                <td><input type="text" class="box"  name="purchase_from" value="<?php echo $all_item_by_id->purchase_from ?>" id="doj"></td>
                            </tr>
                            <tr class="">
                                <td><label for="ex" class="control-label">Reference</label></td>
                                <td>
                                    <input type="text" class="box" maxlength="2" name="reference" value="<?php echo $all_item_by_id->reference ?>" id="ex">
                                </td>
                            </tr>
                            <tr class="">
                                <td><label for="qf" class="control-label">Supplier</label></td>
                                <td><input type="text" class="box"  name="supplier" value="<?php echo $all_item_by_id->supplier ?>" id="qf"></td>
                            </tr>
                            <tr class="">
                                <td><label for="dg" class="control-label">Contact number</label></td>
                                <td><input type="number" class="box" name="contact_number" value="<?php echo $all_item_by_id->contact_number ?>" id="dg"></td>
                            </tr>
                            <tr class="">
                                <td><label for="dg" class="control-label">Email</label></td>
                                <td><input type="email" class="box" required name="email" value="<?php echo $all_item_by_id->email ?>" id="dg"></td>
                            </tr>
                            <tr class="">
                                <td><label class="control-label"></label></td>
                                <td><input type="checkbox" required name="agree">&nbsp; &nbsp;All Information is correct</td>
                            </tr>
                            <tr class="">
                                <td colspan="2">
                                    <button type="submit" class="btn btn-info btn-block">Update Item</button>
                                    <button type="reset" class="btn btn-block btn-default">Cancel</button>
                                </td>
                            </tr>
                        </table>
                   
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
