<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Enterprise Resource Planning</title>  
        <style type="text/css">
            .active { background-color: green; }
            .container{
                width: 100%;
                margin: 0 auto;
            }
        </style>
    </head>

    <body class="bk"><br/>
        <div class="container">
            <div class="panel panel-default">
                <div class="panel-body bkbl">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-12" style="background-color: olivedrab; color: #fff">
                                Invoice View
                            </div>
                            <table class="table table-responsive table-bordered">
                                <tr>
                                    <td>
                                        <mark><strong>Invoice TO</strong></mark> <br/>
                                        <strong> Customer ID </strong># <br/>
                                        <strong> Customer Name</strong> #<br/>
                                        <strong>Customer Address</strong> #<br/>
                                        <strong> Customer Email</strong> #<br/>
                                        <strong> Customer Phone</strong> # <br/>
                                    </td>
                                    <td><br/>
                                        <?php echo $order_info->customer_id ?><br/>
                                        <?php echo $order_info->organization_name ?><br/>
                                        <?php echo $order_info->address ?><br/>
                                        <?php echo $order_info->email ?><br/>
                                        <?php echo $order_info->phone ?><br/>  
                                    </td>
                                    <td>
                                        <strong><strong>Invoice No :INV</strong>#<?php echo $order_info->order_id ?></strong><br/>  <br/>
                                        <strong>Reccived By</strong> &nbsp;&nbsp;:<?php echo $order_info->first_name ?>&nbsp;<?php echo $order_info->last_name ?><br/>  <br/>
                                        <strong>Invoice Date</strong>&nbsp;&nbsp; :<?php echo $order_info->order_date_time ?><br/>  

                                    </td>
                                </tr>
                            </table>
                        </div>  
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-12" style="background-color: olivedrab; color: #fff">
                                Order Details
                            </div>
                            <table class="" border="1" width="100%">
                                <tr>
                                    <th>Product ID</th>
                                    <th>Product Name</th>
                                    <th>Unit Price</th>
                                    <th>Discount</th>
                                    <th>Vat</th>
                                    <th>Price Discount +Vat</th>
                                    <th>Quantity</th>
                                    <th>Sub Total</th>
                                </tr>
                                <?php
                                $total = 0;
                                foreach ($order_full_details as $v_p) {
                                    ?>
                                    <tr>
                                        <td><?php echo $v_p->product_id ?></td>  
                                        <td><?php echo $v_p->product_name ?></td>  
                                        <td><?php echo $v_p->product_price_per_quantity ?></td>  
                                        <td><?php echo $v_p->discount ?></td>  
                                        <td><?php echo $v_p->vat ?></td>  
                                        <td><?php echo $v_p->price_after_vat ?></td>  
                                        <td><?php echo $v_p->product_sales_quantity ?></td>   
                                        <td><?php
                                            echo $v_p->price_after_vat * $v_p->product_sales_quantity;
                                            ?>
                                        </td>  
                                    </tr>
                                    <?php $total = $total + $v_p->price_after_vat * $v_p->product_sales_quantity; ?>
                                <?php } ?>
                                <tr class="success">
                                    <td colspan="6"></td>
                                    <td style="background-color:#D9EDF7">Order Total :</td>
                                    <td style="background-color:#D9EDF7">
                                        <?php
                                        echo $total;
                                        ?>
                                    </td>
                                </tr>
                                <tr class="success">
                                    <td colspan="6"></td>
                                    <td>Order Discount :</td>
                                    <td>
                                        <?php
                                        $dis = ($order_info->discount * $total) / 100;
                                        echo $dis;
                                        ?>
                                    </td>
                                </tr>
                                <tr class="success">
                                    <td colspan="6"></td>
                                    <td>Order Vat:</td>
                                    <td>
                                        <?php
                                        $vat = ($order_info->vat * $total) / 100;
                                        echo $vat;
                                        ?>
                                    </td>
                                </tr>
                                <tr class="success">
                                    <td colspan="6"></td>
                                    <td style="background-color:#D9EDF7">Grand Total :</td>
                                    <td style="background-color:#D9EDF7">
                                        <?php
                                        $dis = ($order_info->discount * $total) / 100;
                                        $vat = ($order_info->vat * $total) / 100;
                                        $grandtotal = ($total - $dis) + $vat;
                                        echo $grandtotal;
                                        ?>
                                    </td>
                                </tr>
                                <tr class="success">
                                    <td colspan="6"></td>
                                    <td><mark>Payment Type</mark>:</td>
                                    <td>
                                        <mark> <?php
                                            echo $order_info->payment_type;
                                            ?></mark>
                                    </td>
                                </tr>
                                <tr class="success">
                                    <td colspan="6"></td>
                                    <td>Pay Amount :</td>
                                    <td>
                                        <?php
                                        echo $order_info->cash;
                                        ?>
                                    </td>
                                </tr>
                                <tr class="success">
                                    <td colspan="6"></td>
                                    <td>Due Amount :</td>
                                    <td>
                                        <?php
                                        $due = $grandtotal - $order_info->cash;
                                        if ($due <= 0) {
                                            echo "Paid";
                                        } else {
                                            if ($order_info->payment_type == "bank") {
                                                echo "Paid";
                                            } else {
                                                echo $due;
                                            }
                                        }
                                        ?>
                                    </td>
                                </tr>
                            </table>
                        </div>  
                    </div>

                    <div style="background-color: #6B8E22; color: #fff;
                         height: 40px;
                         line-height:20px">
                        <div class="cl"><center><span class="pull-left">&copy;&nbsp;Copyright rashedkhan.com </span><span>2015</span></center></div>
                    </div>
                </div>

            </div>
        </div>

    </body>
</html>