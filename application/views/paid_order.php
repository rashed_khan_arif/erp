<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <br/>
        </div>
        <?php
        $msg = $this->session->userdata('msg');
        if ($msg) {
            echo "<script>alert('$msg')</script>";
            $this->session->unset_userdata('msg');
        }
        ?>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <form action="<?php echo base_url() ?>admin/update_order" method="post">
                        <table class="table table-bordered table-hover table-striped text-center tbl_color">
                            <thead>
                                <tr class="success">
                                    <th class="text-center">Paid this order</th>    
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td></td>                                       
                                </tr>    
                                <tr>    
                            <input type="hidden" class="" value="<?Php echo $select_unpaid_order->payment_id ?>" name="payment_id">   
                            <td><input type="text" class="" name="cash"></td>        
                            </tr>    
                            <tr>    
                                <td><input type="submit" class="btn" value="pay" ></td>        
                            </tr>    
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>