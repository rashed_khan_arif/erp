<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Enterprise Resource Planning</title>  
    <link href="<?php echo base_url(); ?>asset/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>asset/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>asset/css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>   
    <link href="<?php echo base_url(); ?>asset/css/style.css" rel="stylesheet" type="text/css"/>

</head>
<body>
    <div class="container">
        <div class="panel panel-default">

            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12" id="printarea">
                        <table class="table table-responsive table-bordered table-hover table-striped tbl_color">

                            <tr class="info text-center">
                                <td colspan="13"><span>Employee Information</span></td>
                            </tr>
                            <tr>
                                <td>Employee ID:</td>
                                <td><?php echo $employee->employee_id ?></td>
                            </tr>
                            <tr>
                                <td>Employee Name:</td>
                                <td><?php echo $employee->first_name ?>&nbsp;&nbsp;&nbsp;<?php echo $employee->last_name ?></td>
                            </tr>
                            <tr>
                                <td>Employee Designation:</td>
                                <td><?php echo $employee->designation ?></td>
                            </tr>
                            <tr>
                                <td>Employee Status:</td>
                                <td><?php
                                    if ($employee->status == 1) {
                                        echo "Active";
                                    } else {
                                        echo "Inactive";
                                    }
                                    ?></td>
                            </tr>
                            <tr>
                                <td>Employee Job Status:</td>
                                <td><?php echo $employee->job_status ?></td>
                            </tr>

                        </table>      
                        <table class="table table-responsive table-bordered table-hover table-striped text-center tbl_color">  
                            <thead>
                                <tr class="info">
                                    <td colspan="13"><span>Employee Salary Information</span></td>
                                </tr>
                                <tr class="success">
                                    <th class="text-center">Month</th>
                                    <th class="text-center">Year</th>
                                    <th class="text-center">Basic</th>
                                    <th class="text-center">Incre</th>
                                    <th class="text-center">H_Rent</th>
                                    <th class="text-center">Conv</th>
                                    <th class="text-center">Medical</th>
                                    <th class="text-center">Gross</th>
                                    <th class="text-center">Overtime</th>
                                    <th class="text-center">Annual</th>
                                    <th class="text-center">Income Tax</th>
                                    <th class="text-center">Deduction</th>
                                    <th class="text-center">Total</th>                                
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $total = 0;
                                foreach ($employee_salary as $v_sal) {
                                    ?>
                                    <tr>
                                        <td><?php echo $v_sal->month ?></td>
                                        <td><?php echo $v_sal->year ?></td>
                                        <td><?php echo $v_sal->basic_salary ?></td>
                                        <td><?php echo $v_sal->increment ?></td>
                                        <td><?php echo $v_sal->house_rent ?></td>
                                        <td><?php echo $v_sal->conveyance ?></td>
                                        <td><?php echo $v_sal->medical ?></td>
                                        <td><?php echo $v_sal->gross ?></td>
                                        <td><?php echo $v_sal->overtime ?></td>
                                        <td><?php echo $v_sal->annual ?></td>
                                        <td><?php echo $v_sal->income_tax ?></td>
                                        <td><?php echo $v_sal->deduction ?></td>
                                        <td><?php echo $v_sal->total ?></td>
                                    </tr>
                                    <?php $total = $total + $v_sal->total ?>
                                <?php } ?>
                                <tr>
                                    <td colspan="12" class="text-right">
                                        Grand Total :
                                    </td>
                                    <td>
                                        <?php echo $total; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="13" class="text-right">
                                         <p class="text-center">&copy; Rashed Khan Arif</p>
                                    </td>
                                </tr>
                            <script type="text/javascript">
                                function printDi(printarea) {
                                    var printContents = document.getElementById(printarea).innerHTML;
                                    var originalContents = document.body.innerHTML;

                                    document.body.innerHTML = printContents;

                                    window.print();

                                    document.body.innerHTML = originalContents;
                                }
                            </script>

                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-12">
                        <input type="button" onclick="printDi('printarea')" value="Print" class="btn btn-block btn-info">
                    </div>                 
                </div>
            </div>
        </div>
    </div>
</body>
</html>