<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Enterprise Resource Planning</title>  
        <link href="<?php echo base_url(); ?>asset/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>asset/css/style.css" rel="stylesheet" type="text/css"/>
        <script src="<?php echo base_url() ?>asset/js/jquery-1.4.2.min.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" media="screen" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/base/jquery-ui.css">
        <link class="jsbin" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
        <script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
        <script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.0/jquery-ui.min.js"></script>
    </head>
    <body>
        <div class="container">
            <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand page-scroll" href="#page-top">Admin</a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right" id="m"> 
                            <li role="presentation"><a href="<?php echo base_url() ?>admin">Home</a></li>
                            <li><a href="<?php echo base_url() ?>admin/view_employee" class="page-scroll">Employee</a></li>
                            <li><a href="<?php echo base_url() ?>admin/salary" class="page-scroll">Salary</a></li>
                            <li><a href="<?php echo base_url() ?>admin/asset" class="page-scroll">Asset</a></li>
                            <li><a href="<?php echo base_url() ?>admin/customer" class="page-scroll">Customer</a></li>
                            <li><a href="<?php echo base_url() ?>admin/storein" class="page-scroll">Stoke In</a></li>
                            <li><a href="<?php echo base_url() ?>admin/order" class="page-scroll">Order</a></li>
                            <li><a href="<?php echo base_url() ?>admin/payment" class="page-scroll">Payment</a></li>
                            <li><a href="<?php echo base_url() ?>admin/storeout" class="page-scroll">Store Out</a></li>
                            <li><a href="<?php echo base_url() ?>admin/report" class="page-scroll">Report</a></li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container-fluid -->
            </nav>
        </div>
        <br/>
        <br/>
        <br/>
        <section class="bg-default" id="about">
            <?php echo $main_content ?>
        </section>
        
        <script src="<?php echo base_url() ?>asset/js/jquery-1.4.2.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(function () {
                // this will get the full URL at the address bar
                var url = window.location.href;

                // passes on every "a" tag 
                $("#m a").each(function () {
                    // checks if its the same on the address bar
                    if (url == (this.href)) {
                        $(this).closest("li").addClass("active");
                    }
                });
            });
        </script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="<?php echo base_url() ?>asset/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>asset/js/jquery-1.4.2.min.js" type="text/javascript"></script>

    </body>

</html>