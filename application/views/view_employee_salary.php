<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <a href="<?php echo base_url() ?>admin/salary_entry" class="btn btn-info">Salary Entry</a>
            <a href="<?php echo base_url() ?>admin/salary_statement" class="btn btn-info">Salary Statement</a>
            <a href="<?php echo base_url() ?>admin/view_pay_salary" class="btn btn-info">View Pay Salary</a>
            <a href="<?php echo base_url() ?>admin/salary" class="btn btn-info">View Salary</a>

        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <table  id="printarea" class="table table-responsive table-bordered table-hover table-striped tbl_color">

                        <tr class="info text-center">
                            <td colspan="13"><span>Employee Information</span></td>
                        </tr>
                        <tr>
                            <td>Employee ID:</td>
                            <td><?php echo $employee->employee_id ?></td>
                        </tr>
                        <tr>
                            <td>Employee Name:</td>
                            <td><?php echo $employee->first_name ?>&nbsp;&nbsp;&nbsp;<?php echo $employee->last_name ?></td>
                        </tr>
                        <tr>
                            <td>Employee Designation:</td>
                            <td><?php echo $employee->designation ?></td>
                        </tr>
                        <tr>
                            <td>Employee Status:</td>
                            <td><?php
                                if ($employee->status == 1) {
                                    echo "Active";
                                } else {
                                    echo "Inactive";
                                }
                                ?></td>
                        </tr>
                        <tr>
                            <td>Employee Job Status:</td>
                            <td><?php echo $employee->job_status ?></td>
                        </tr>

                    </table>      
                    <table class="table table-responsive table-bordered table-hover table-striped text-center tbl_color">  
                        <thead>
                            <tr class="info">
                                <td colspan="14"><span>Employee Salary Information</span></td>
                            </tr>
                            <tr class="success">
                                <th class="text-center">Month</th>
                                <th class="text-center">Year</th>
                                <th class="text-center">Basic</th>
                                <th class="text-center">Incre</th>
                                <th class="text-center">H_Rent</th>
                                <th class="text-center">Conv</th>
                                <th class="text-center">Medical</th>
                                <th class="text-center">Gross</th>
                                <th class="text-center">Overtime</th>
                                <th class="text-center">Annual</th>
                                <th class="text-center">Income Tax</th>
                                <th class="text-center">Deduction</th>
                                <th class="text-center">Total</th>                                
                                <th class="text-center">Payment Status</th>                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $total = 0;
                            foreach ($employee_salary as $v_sal) {
                                ?>
                                <tr>
                                    <td><?php echo $v_sal->month ?></td>
                                    <td><?php echo $v_sal->year ?></td>
                                    <td><?php echo $v_sal->basic_salary ?></td>
                                    <td><?php echo $v_sal->increment ?></td>
                                    <td><?php echo $v_sal->house_rent ?></td>
                                    <td><?php echo $v_sal->conveyance ?></td>
                                    <td><?php echo $v_sal->medical ?></td>
                                    <td><?php echo $v_sal->gross ?></td>
                                    <td><?php echo $v_sal->overtime ?></td>
                                    <td><?php echo $v_sal->annual ?></td>
                                    <td><?php echo $v_sal->income_tax ?></td>
                                    <td><?php echo $v_sal->deduction ?></td>
                                    <td><?php echo $v_sal->total ?></td>
                                    <?php
                                    foreach ($employee_pay_salary as $v_psal) {
                                        if ($v_sal->si_id === $v_psal->si_id) {
                                            ?>
                                            <td>
                                                <?php
                                                echo "<a class='btn btn-block btn-info btn-sm'>Paid</a>";
                                                ?>
                                            </td>  
                                        <?php } ?>

                                    <?php } ?>
                                </tr>

                                <?php $total = $total + $v_sal->total ?>
                            <?php } ?>
                            <tr>
                                <td colspan="12" class="text-right">
                                    Grand Total :
                                </td>
                                <td>
                                    <?php echo $total; ?>
                                </td>
                            </tr>

                        <script type="text/javascript">
                            function printDi(printarea) {
                                var printContents = document.getElementById(printarea).innerHTML;
                                var originalContents = document.body.innerHTML;

                                document.body.innerHTML = printContents;

                                window.print();

                                document.body.innerHTML = originalContents;
                            }
                        </script>
                        <tr>
                            <td colspan="8" class="text-right">
                                <a href="<?php echo base_url() ?>admin/print_employee_salary_info/<?php echo $v_sal->emp_id ?>" target="_blank" class="btn btn-block btn-info">Print</a>
                            </td>
                            <td colspan="6" class="text-left">
                                <a href="<?php echo base_url() ?>admin/create_emp_salary_invoice/<?php echo $v_sal->emp_id ?>" class="btn btn-block btn-info">Save As pdf</a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>