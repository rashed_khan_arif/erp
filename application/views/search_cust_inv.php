<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <a href="<?php echo base_url() ?>admin/order" class="btn btn-info" >Create Invoice</a>
            <a href="" class="btn btn-info" >View Invoice</a>
        </div>
        <div class="panel-body">
            <div class="row">         
                <div class="col-md-12 container">
                    <table class="table table-striped">
                        <?php
                        $msg = $this->session->userdata('msg');
                        if ($msg) {
                            echo "<script>alert('$msg')</script>";
                            $this->session->unset_userdata('msg');
                        }
                        ?>
                        <tr>
                        <form action="<?php echo base_url() ?>checkout/select_customer" method="post">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Customer ID:</td>
                            <td> 
                                <input type="text" class="form-control" id="exampleInputAmount" required name="c_id">
                                <input type="submit" class="btn btn-info" value="Enter">
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </form>
                        </tr>
                    </table>

                </div>

                <div class="col-md-12"> 
                    <div class="col-md-12">
                        <div class="col-md-6 pull-left text-center">
                            <form class="form-inline" action="<?php echo base_url() ?>checkout/search_customer_for_invoice" method="get">
                                <input type="text" class="form-control" id="exampleInputAmount" required name="search" placeholder="Search By Customer ID">
                                <input type="submit" class="btn btn-info" value="Search Customer">
                            </form>
                        </div>
                        <div class="col-md-6 pull-right text-center">


                        </div>
                    </div>
                    <br/>
                    <br/>
                    <div class="row">
                        <?php
                        $msg = $this->session->userdata('msg');
                        if ($msg) {
                            ?>
                            <p style="color: red; font-size: 22px;" class="text-center">
                                <?php
                                echo $msg;
                                $this->session->unset_userdata('msg');
                                ?>
                            </p>
                        <?php } ?>
                        <table class="table table-responsive table-bordered table-hover table-striped text-center tbl_color">
                            <thead>
                                <tr class="success">
                                    <th class="text-center">Customer ID</th>
                                    <th class="text-center">Organization Name</th>
                                    <th class="text-center">Reference</th>
                                    <th class="text-center">Date_of_join</th>

                                    <th class="text-center">Phone</th> 
                                    <th class="text-center">credit</th>
                                    <th class="text-center">Credit Amount</th>       

                                </tr>
                            </thead>
                            <tbody>

                                <?php foreach ($select_custo as $v_em) { ?>
                                    <tr>
                                        <td><?php echo $v_em->customer_id ?></td>
                                        <td><?php echo $v_em->organization_name ?></td>
                                        <td><?php echo $v_em->reference ?></td>
                                        <td><?php echo $v_em->date_of_join ?></td>
                                        <td><?php echo $v_em->phone ?></td>        
                                        <td><?php echo $v_em->credit ?></td>
                                        <td><?php echo $v_em->credit_amount ?></td>   

                                    </tr>
                                <?php } ?>  

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>