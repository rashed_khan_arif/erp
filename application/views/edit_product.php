<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <a href="<?php echo base_url() ?>admin/storein" class="btn btn-info">View Product</a>
            <a href="<?php echo base_url() ?>admin/add_new_product" class="btn btn-info">Add Product</a>
        </div>
        <div class="panel-body ">
            <div class="row">
                <div class="col-md-8 col-md-offset-2" style="border: solid 1px #ccc">   
                    <form action="<?php echo base_url() ?>admin/update_product" method="post">
                        <fieldset>
                            <table class="table table-condensed table-hover table-striped table-bordered">
                                <?php
                                $msg = $this->session->userdata('msg');
                                if ($msg) {
                                    ?>
                                    <tr class="info msg" id="msg">
                                        <td colspan="2" class="text-center"><?php
                                            echo $msg;
                                            $this->session->unset_userdata('msg');
                                            ?></td>
                                    </tr>
                                <?php } ?>

                                <tr class="success msg">
                                    <td colspan="2" class="text-center"><span style="font-size: 25px; font-weight: bold">Update Product</span></td>
                                </tr>
                                <tr class="">
                                    <td><label for="fname" class="control-label">Product ID</label></td>
                                <input type="hidden" value="<?php echo $select_product_id->p_id ?>" class="box" name="p_id" required id="fname">
                                <td><input type="text" value="<?php echo $select_product_id->product_id ?>" class="box" name="product_id" required id="fname"></td>
                                </tr>
                                <tr class="">
                                    <td><label for="lname" class="control-label">Product Name</label></td>
                                    <td><input type="text" value=" <?php echo $select_product_id->product_name ?>" class="box" name="product_name" required id="lname"></td>
                                </tr>
                                <tr class="">
                                    <td><label for="bank" class="control-label">Product Category</label></td>
                                    <td><input type="text"value="<?php echo $select_product_id->product_category ?>" class="box" name="product_category" id="bank"></td>
                                </tr>
                                <tr class="">
                                    <td><label for="dob" class="control-label">Product Sub Category</label></td>
                                    <td>
                                        <input type="text" value="<?php echo $select_product_id->product_sub_category ?>" name="product_sub_category" id="dob" class="box"/>
                                    </td>
                                </tr>
                                <tr class="">
                                    <td><label for="id" class="control-label">Product Description</label></td>
                                    <td>
                                        <textarea type="number" class="box" name="product_description">
                                            <?php echo $select_product_id->product_description ?>
                                        </textarea>
                                    </td>
                                </tr>
                                <tr class="">
                                    <td><label for="ross" class="control-label">Product Quantity</label></td>
                                    <td><input type="text" value="<?php echo $select_product_id->product_quantity ?>" class="box" name="product_quantity" id="ross"></td>
                                </tr>
                                <tr class="">
                                    <td><label for="em" class="control-label">Product Price Per Quantity</label></td>
                                    <td><input type="number" value="<?php echo $select_product_id->product_price_per_quantity ?>" class="box" name="product_price_per_quantity" id="em"></td>
                                </tr>
                                <tr class="">
                                    <td><label for="email" class="control-label">Discount</label></td>
                                    <td><input type="text" value="<?php echo $select_product_id->discount ?>" class="box" name="discount" placeholder="Ex :5%" id="email"></td>
                                </tr>
                                <tr class="">
                                    <td><label for="email" class="control-label">R Discount</label></td>
                                    <td><input type="number" value="<?php echo $select_product_id->rdiscount ?>" class="box" name="rdiscount" id="email"></td>
                                </tr>
                                <tr class="">
                                    <td><label for="email" class="control-label">Price After Discount</label></td>
                                    <td><input type="number" value="<?php echo $select_product_id->price_after_discount ?>" class="box" name="price_after_discount" id="email"></td>
                                </tr>

                                <tr class="">
                                    <td><label for="doj" class="control-label">Vat</label></td>
                                    <td><input type="text" value="<?php echo $select_product_id->vat ?>" class="box"  name="vat" placeholder="Ex :15%" id="doj"></td>
                                </tr>
                                <tr class="">
                                    <td><label for="doj" class="control-label">R Vat</label></td>
                                    <td><input type="number" value="<?php echo $select_product_id->rvat ?>" class="box"  name="rvat" id="doj"></td>
                                </tr>
                                <tr class="">
                                    <td><label for="ex" class="control-label">Price After Vat</label></td>
                                    <td>
                                        <input type="text" value="<?php echo $select_product_id->price_after_vat ?> " class="box" maxlength="2" name="price_after_vat" id="ex">
                                    </td>
                                </tr>
                                <tr class="">
                                    <td><label for="qf" class="control-label">Price</label></td>
                                    <td><input type="number" value="<?php echo $select_product_id->price ?>" class="box"  name="price" id="qf"></td>
                                </tr>
                                <tr class="">
                                    <td><label for="dg" class="control-label">Storing Date</label></td>
                                    <td><input type="date" value="<?php echo $select_product_id->storing_date ?>" class="box" name="storing_date" id="dg"></td>
                                </tr>
                                <tr class="">
                                    <td><label for="dg" class="control-label">Storing Location</label></td>
                                    <td><input type="text" value=" <?php echo $select_product_id->storing_location ?>" class="box" required name="storing_location" id="dg"></td>
                                </tr>
                                <tr class="">
                                    <td><label class="control-label"></label></td>
                                    <td><input type="checkbox" required name="agree">&nbsp; &nbsp;All Information is correct</td>
                                </tr>
                                <tr class="">
                                    <td colspan="2">
                                        <button type="submit" class="btn btn-info btn-block">Update Item</button>
                                        <button type="reset" class="btn btn-block btn-default">Cancel</button>
                                    </td>
                                </tr>
                            </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
