<html>
    <head>
        <style type="text/css">
            .table{
                width: 100%;
               
            }
        </style>
    </head>
    <body>
        <div class="panel panel-default" id="printarea">
            <div class="panel-body bk">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <fieldset>
                            <table class="table" border='0'>
                                <?php
                                $msg = $this->session->userdata('msg');
                                if ($msg) {
                                    ?>
                                    <tr class="info msg" id="msg">
                                        <td colspan="2" class="text-center"><?php
                                            echo $msg;
                                            $this->session->unset_userdata('msg');
                                            ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                <tr class="info msg">
                                    <td colspan="2" align='center'>
                                        <span style="font-size: 30px">Customer Details</span>
                                        <hr/>
                                <hr/><br/>
                                    </td>
                               
                                </tr>
                                <tr class="info">
                                    <td><label for="fname" class="control-label">customer ID</label></td>
                                <input type="hidden" class="box" name="item_no" required id="fname">
                                <td>
                                    <?php echo $select_customer->customer_id ?>
                                </td>
                                </tr>
                                <tr class="info">
                                    <td><label for="lname" class="control-label">Organization name</label></td>
                                    <td>
                                        <?php echo $select_customer->organization_name ?>
                                    </td>
                                </tr>
                                <tr class="info">
                                    <td><label for="bank" class="control-label">Reference</label></td>
                                    <td>
                                        <?php echo $select_customer->reference ?>
                                    </td>
                                </tr>
                                <tr class="info">
                                    <td><label for="dob" class="control-label">Date of Join</label></td>
                                    <td>
                                        <?php echo $select_customer->date_of_join ?>
                                    </td>
                                </tr>
                                <tr class="info">
                                    <td><label for="id" class="control-label">Address</label></td>
                                    <td><?php echo $select_customer->address ?></td>
                                </tr>

                                <tr class="info">
                                    <td><label for="em" class="control-label">phone</label></td>
                                    <td><?php echo $select_customer->phone ?></td>
                                </tr>
                                <tr class="info">
                                    <td><label for="email" class="control-label">Email</label></td>
                                    <td><?php echo $select_customer->email ?></td>
                                </tr>
                                <tr class="info">
                                    <td><label class="control-label">Credit</label></td>
                                    <td><?php echo $select_customer->credit ?></td>
                                </tr>         
                                <tr class="info">
                                    <td><label class="control-label">Credit Amount</label></td>
                                    <td><?php echo $select_customer->credit_amount ?></td>
                                </tr>
                                <tr class="info">
                                    <td><label for="doj" class="control-label">Shipment Address</label></td>
                                    <td><?php echo $select_customer->shipment_address ?></td>
                                </tr>   
                            </table>

                        </fieldset>

                    </div>
                </div>
            </div>
        </div>