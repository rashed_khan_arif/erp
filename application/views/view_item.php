<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <a href="<?php echo base_url() ?>admin/add_new_item" class="btn btn-info pull-left">Add New Item</a> 
            <form class="form-inline" action="<?php echo base_url() ?>admin/search_item" method="get">
                <input type="submit" class="btn btn-info pull-right" value="Search">
                <input type="text" class="form-control pull-right" required  name="search" placeholder="Type item ID and Enter">
            </form><br/><br/>
        </div>
        <div class="panel-body">
            <div class="row">
                 <div class="col-md-12">
                <table class="table table-responsive table-bordered table-hover table-striped text-center tbl_color">
                    <thead>
                        <tr class="success">
                            <th class="text-center">Item ID</th>
                            <th class="text-center">Category</th>           
                            <th class="text-center">Quantity</th>  
                            <th class="text-center">Invoice no</th>                
                            <th class="text-center">Date of purchase</th>
                            <th class="text-center">Purchase from</th>
                            <th class="text-center">Reference</th>
                            <th class="text-center">Supplier</th>
                            <th class="text-center">Contact number</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($all_item as $v_item) { ?>
                            <tr>
                                <td><?php echo $v_item->item_id ?></td>
                                <td><?php echo $v_item->category ?></td>
                                <td><?php echo $v_item->quantity ?></td>                    
                                <td><?php echo $v_item->invoice_no ?></td>  
                                <td><?php echo $v_item->date_of_purchase ?></td>
                                <td><?php echo $v_item->purchase_from ?></td>
                                <td><?php echo $v_item->reference ?></td>
                                <td><?php echo $v_item->supplier ?></td>
                                <td><?php echo $v_item->contact_number ?></td>
                                <td>
                                    <a class="btn btn-info btn-sm" title="Click To Edit" href="<?php echo base_url() ?>admin/view_items/<?php echo $v_item->item_no ?>"> <li class="glyphicon glyphicon-eye-open"></li></a>
                                    <a class="btn btn-success btn-sm" title="Click To Edit" href="<?php echo base_url() ?>admin/edit_item/<?php echo $v_item->item_no ?>"> <li class="glyphicon glyphicon-edit"></li></a>
                                    <a class="btn btn-danger btn-sm" title="Click To Delete" onclick="return check();" href="<?php echo base_url() ?>admin/delete_item/<?php echo $v_item->item_no; ?>"> <li class="glyphicon glyphicon-trash"></li></a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            </div>
        </div>
    </div>
</div>