<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <a href="<?php echo base_url() ?>admin/salary_entry" class="btn btn-info">Salary Entry</a>
            <a href="<?php echo base_url() ?>admin/salary_payment" class="btn btn-info">Make Payment</a>
            <a href="<?php echo base_url() ?>admin/salary_statement" class="btn btn-info">Salary Statement</a>
            <a href="<?php echo base_url() ?>admin/view_pay_salary" class="btn btn-info">View Pay Salary</a>
        </div>
        <?php
        $msg = $this->session->userdata('msg');
        if ($msg) {
            echo "<script>alert('$msg')</script>";
            $this->session->unset_userdata('msg');
        }
        ?>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-responsive table-bordered table-hover table-striped text-center tbl_color">
                        <thead>
                            <tr class="success"> 
                                <th class="text-center">Employee ID</th>
                                <th class="text-center">Employee Name</th>
                                <th class="text-center">Designation</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Joining Date</th>
                                <th class="text-center">Contact</th> 
                                <th class="text-center">Last Month</th> 
                                <th class="text-center">Year</th> 
                                <th class="text-center">Total</th> 
                                <th class="text-center">View</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($employee_salary as $v_e) { ?>
                                <tr>
                                    <td><?php echo $v_e->employee_id ?></td>
                                    <td><?php echo $v_e->first_name ?>&nbsp;<?php echo $v_e->last_name ?></td>
                                    <td><?php echo $v_e->designation ?></td>
                                    <td><?php
                                        if ($v_e->status == 1) {
                                            echo "Active";
                                        } else {
                                            echo "Inactive";
                                        }
                                        ?></td>
                                    <td><?php echo $v_e->doj ?></td>
                                    <td><?php echo $v_e->contact ?></td>
                                    <td><?php echo $v_e->month ?></td>
                                    <td><?php echo $v_e->year ?></td>
                                    <td><?php echo $v_e->total ?></td>

                                    <td>
                                        <a class="btn btn-info btn-sm" title="Click To View" href="<?php echo base_url() ?>admin/single_salary/<?php echo $v_e->emp_id ?>"> <li class="glyphicon glyphicon-eye-open">View</li></a>       
                                    </td>
                                    <td>
                                        <a class="btn btn-primary btn-sm" title="Click To Edit" href="<?php echo base_url() ?>admin/edit_salary/<?php echo $v_e->si_id ?>"> <li class="glyphicon glyphicon-edit">Edit</li></a>       
<!--                                        <a class="btn btn-danger btn-sm" title="Click To del" href="<?php //echo base_url() ?>admin/delete_salary/<?php// echo $v_e->emp_id ?>"> <li class="glyphicon glyphicon-trash">Delete</li></a>  -->     
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>