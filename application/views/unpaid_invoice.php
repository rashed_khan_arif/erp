<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <a href="<?php echo base_url() ?>admin/view_invoice" class="btn btn-info pull-left" >View Invoice</a>             

            <?php if ($this->session->userdata('c_id')) { ?>
                <a href="<?php echo base_url() ?>cart/show_cart" class="btn btn-info pull-left" >Complete Last order</a>             
                <a href="<?php echo base_url() ?>admin/logout_customer" class="btn btn-info pull-left" >Cancel Last order</a>             
            <?php } ?>       
            <form class="form-inline" action="<?php echo base_url() ?>admin/search_order" method="get">
                <input type="submit" class="btn btn-info pull-right" value="Search Invoice">
                <input type="text" class="form-control pull-right" required name="search" placeholder="Search By Invoice ID">
            </form>
            <br/><br/>
        </div>
        <?php
        $msg = $this->session->userdata('msg');
        if ($msg) {
            echo "<script>alert('$msg')</script>";
            $this->session->unset_userdata('msg');
        }
        ?>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">


                    <table class="table table-bordered table-hover table-striped text-center tbl_color">
                        <thead>
                            <tr class="success">
                                <th class="text-center">Invoice ID</th>   
                                <th class="text-center">Customer ID</th>  
                                <th class="text-center">Customer Name</th>  
                                <th class="text-center">Order Total</th>     
                                <th class="text-center">Order Date Time</th>     
                                <th class="text-center">Action</th>            
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($all_unpaid_order as $v_em) {
                                ?>
                                <tr>
                                    <td><?php echo $v_em->order_id ?></td>                
                                    <td><?php echo $v_em->customer_id ?></td>   
                                    <td><?php echo $v_em->organization_name ?></td>   
                                    <td><?php echo $v_em->order_total ?></td>   
                                    <td><?php echo $v_em->order_date_time ?></td>   
                                    <td>
                                        <a href="<?php echo base_url() ?>admin/paid_order/<?php echo $v_em->order_id ?>" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Click to paid this"><li class="glyphicon">Paid this </li></a>
                                    </td>                         
                                </tr>
                            <?php } ?>  
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>