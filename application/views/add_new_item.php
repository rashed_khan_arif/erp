<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <a href="<?php echo base_url() ?>admin/view_item" class="btn btn-info">View item</a>

        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-8 col-md-offset-2" style="border: solid 1px #ccc">
                    <div class="container centered">
                    </div><br/>
                    <form action="<?php echo base_url() ?>admin/save_item" method="post">
                        <fieldset>
                            <table class="table table-condensed table-hover table-striped table-bordered">
                                <?php
                                $msg = $this->session->userdata('msg');
                                if ($msg) {
                                    ?>
                                    <tr class="info msg" id="msg">
                                        <td colspan="2" class="text-center"><?php
                                            echo $msg;
                                            $this->session->unset_userdata('msg');
                                            ?></td>
                                    </tr>
                                <?php } ?> 
                                <tr class="success msg">
                                    <td colspan="2" class="text-center"><span style="font-size: 25px">Add New Item</span></td>
                                </tr>

                                <tr class="">
                                    <td><label for="fname" class="control-label">Item ID</label></td>
                                    <td><input type="text" class="box" name="item_id" required id="fname"></td>
                                </tr>
                                <tr class="">
                                    <td><label for="lname" class="control-label">Category</label></td>
                                    <td><input type="text" class="box" name="category" required id="lname"></td>
                                </tr>
                                <tr class="">
                                    <td><label for="bank" class="control-label">Sub category</label></td>
                                    <td><input type="text" class="box" name="sub_category" id="bank"></td>
                                </tr>
                                <tr class="">
                                    <td><label for="dob" class="control-label">Description</label></td>
                                    <td>
                                        <textarea name="description" class="box"></textarea>

                                    </td>
                                </tr>
                                <tr class="">
                                    <td><label for="id" class="control-label">Quantity</label></td>
                                    <td><input type="number" class="box" name="quantity" id="id"></td>
                                </tr>
                                <tr class="">
                                    <td><label for="ross" class="control-label">Type</label></td>
                                    <td><input type="text" class="box" name="type" id="ross"></td>
                                </tr>
                                <tr class="">
                                    <td><label for="em" class="control-label">Price per quantity</label></td>
                                    <td><input type="text" class="box" name="price_per_quantity" id="em"></td>
                                </tr>
                                <tr class="">
                                    <td><label for="email" class="control-label">Invoice no</label></td>
                                    <td><input type="text" class="box" name="invoice_no" id="email"></td>
                                </tr>
                                <tr class="">
                                    <td><label class="control-label">Invoice details location</label></td>
                                    <td><input type="text" class="box"  id="datepicker" name="invoice_details_location" ></td>
                                </tr>         
                                <tr class="">
                                    <td><label class="control-label">Date of purchase</label></td>
                                    <td><input type="date" placeholder="mm/dd/yyy" class="box" class="datepicker" name="date_of_purchase"  ></td>
                                </tr>
                                <tr class="">
                                    <td><label for="doj" class="control-label">Purchase from</label></td>
                                    <td><input type="text" class="box"  name="purchase_from" id="doj"></td>
                                </tr>
                                <tr class="">
                                    <td><label for="ex" class="control-label">Reference</label></td>
                                    <td>
                                        <input type="text" class="box" maxlength="2" name="reference" id="ex">
                                    </td>
                                </tr>
                                <tr class="">
                                    <td><label for="qf" class="control-label">Supplier</label></td>
                                    <td><input type="text" class="box"  name="supplier" id="qf"></td>
                                </tr>
                                <tr class="">
                                    <td><label for="dg" class="control-label">Contact number</label></td>
                                    <td><input type="number" class="box" name="contact_number" id="dg"></td>
                                </tr>
                                <tr class="">
                                    <td><label for="dg" class="control-label">Email</label></td>
                                    <td><input type="email" class="box" required name="email" id="dg"></td>
                                </tr>
                                <tr class="">
                                    <td><label class="control-label"></label></td>
                                    <td><input type="checkbox" required name="agree">&nbsp; &nbsp;All Information is correct</td>
                                </tr>
                                <tr class="">
                                    <td colspan="2">
                                        <button type="submit" class="btn btn-info btn-block">Save Item</button>
                                        <button type="reset" class="btn btn-block btn-default">Cancel</button>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
