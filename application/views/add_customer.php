<div class="container">
<div class="panel panel-default">
    <div class="panel-heading">
        <a href="<?php echo base_url() ?>admin/view_customer" class="btn btn-info">View Customer</a>    
    </div>
    <?php
    $msg = $this->session->userdata('msg');
    if ($msg) {
        echo "<script>alert('$msg')</script>";
        $this->session->unset_userdata('msg');
    }
    ?>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-8 col-md-offset-2" style="border: solid 1px #ccc">
                <form action="<?php echo base_url() ?>admin/save_customer" method="post">
                    <fieldset>
                        <table class="table table-condensed table-hover table-striped table-bordered">
                         
                            <tr class="success">
                                <td colspan="2" class="text-center"><span style="font-size: 30px">Add New Customer</span></td>
                            </tr>

                            <tr class="">
                                <td><label for="fname" class="control-label">Customer ID</label></td>
                                <td><input type="text" class="box" name="customer_id" required id="fname"></td>
                            </tr>
                            <tr class="">
                                <td><label for="lname" class="control-label">Organization Name</label></td>
                                <td><input type="text" class="box" name="organization_name" required id="lname"></td>
                            </tr>
                            <tr class="">
                                <td><label for="bank" class="control-label">Reference</label></td>
                                <td><input type="text" class="box" name="reference" id="bank"></td>
                            </tr>
                            <tr class="">
                                <td><label for="dob" class="control-label">Date of Join</label></td>
                                <td><input type="date" class="box"  name="date_of_join" id="dob"></td>
                            </tr>
                            <tr class="">
                                <td><label for="id" class="control-label">Address</label></td>
                                <td><input type="text" class="box" name="address" id="id"></td>
                            </tr>
                            <tr class="">
                                <td><label for="ross" class="control-label">Phone</label></td>
                                <td><input type="text" class="box" name="phone" id="ross"></td>
                            </tr>
                            <tr class="">
                                <td><label for="em" class="control-label">Email</label></td>
                                <td><input type="text" class="box" name="email" id="em"></td>
                            </tr>
                            <tr class="">
                                <td><label for="email" class="control-label">Credit</label></td>
                                <td>
                                    <input type="radio" onclick="off()"  name="credit" value="Limited">&nbsp;&nbsp;Limited
                                    <input type="radio" onclick="on()" name="credit" value="Un Limited">&nbsp;&nbsp;Unlimited
                                </td>
                            </tr>
                            <script type="text/javascript">
                                function off() {
                                    document.getElementById("hidethis").style.display = '';
                                }
                                function on() {
                                    document.getElementById("hidethis").style.display = '';
                                }
                            </script>
                            <tr class="" id="hidethis" style="display: none">
                                <td><label class="control-label">Credit Amount</label></td>
                                <td><input type="number" class="box" name="credit_amount" ></td>
                            </tr>         
                            <tr class="">
                                <td><label class="control-label">Shipment Address</label></td>
                                <td>
                                    <textarea name="shipment_address" class="box"></textarea>
                                </td>
                            </tr>
                            <tr class="">
                                <td><label class="control-label"></label></td>
                                <td><input type="checkbox" required name="agree">&nbsp; &nbsp;All Information is correct</td>
                            </tr>
                            <tr class="">
                                <td colspan="2">
                                    <button type="submit" class="btn btn-info btn-block">Save Customer Information</button>
                                    <button type="reset" class="btn btn-block btn-default">Cancel</button>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
