<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <a href="<?php echo base_url() ?>admin/view_invoice" class="btn btn-info pull-left" >View Invoice</a>&nbsp;  
            <br/><br/>
        </div>

        <div class="panel-body">
            <div class="row">

                <div class="col-md-12">
                    <table class="table table-bordered table-hover table-striped text-center tbl_color">
                        <thead>
                            <tr class="success">

                                <th class="text-center">Remove</th>   
                                <th class="text-center">Product Name</th>               
                                <th class="text-center">Discount</th>                  
                                <th class="text-center">Vat</th>     
                                <th class="text-center">Unit Price</th> 
                                <th class="text-center">Quantity</th>            
                                <th class="text-center">Sub Total</th>            

                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $content = $this->cart->contents();
                            foreach ($content as $v_em) {
                                ?>
                                <tr>
                                    <td><a href="<?php echo base_url() ?>cart/delete_form_cart/<?php echo $v_em['rowid'] ?>"><span class="glyphicon glyphicon-trash"></span></a></td>                
                                    <td><?php echo $v_em['name'] ?></td>        
                                    <td><?php echo $v_em['discount'] ?>%</td>   
                                    <td><?php echo $v_em['vat'] ?>%</td>     
                                    <td><?php echo $v_em['price'] ?></td>   
                                    <td><form action="<?php echo base_url() ?>cart/update_cart/<?php echo $v_em['rowid'] ?>" method="post"><input type="number" value="<?php echo $v_em['qty'] ?>" name="qty"><button class="btn btn-sm btn-info" type="submit">Update</button></form></td>       
                                    <td><?php echo $v_em['subtotal'] ?></td>   
                                </tr>
                            <?php } ?>  

                        </tbody>
                    </table>   
                    <div class="col-md-12">              
                        <div class="col-md-7">

                        </div>
                        <form action="<?php echo base_url(); ?>checkout/save_invoice" method="post">

                            <div class="col-md-5 text-center">
                                <table class="table table-bordered">
                                    <tr class="success">
                                        <td colspan="2">Total</td>
                                        <td>
                                            <?php
                                            echo $this->cart->total();
                                            $total = $this->cart->total();
                                            $vat = (15 * $total) / 100;
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="checkbox" id="chk1" >
                                        </td>
                                        <td>Discount</td>
                                        <td colspan="2">
                                            <input type="number" class="form-control" id="dis"pattern="\w{2}" title="Enter 2 characters" placeholder="%" name="discount">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="checkbox" id="chk2" >
                                        </td>
                                        <td>Vat</td>
                                        <td colspan="2">
                                            <input type="number" class="form-control" maxlength="2" id="vat" placeholder="%" name="vat">
                                        </td>
                                    </tr>

                                    <tr class="success">
                                        <td colspan="2">Grand Total:</td>
                                        <td>
                                            <?php
                                            $g_t = $this->cart->total();// + $vat;
                                            ?>
                                            <input type="text" readonly class="form-control" name="g_t" value="<?php echo $g_t; ?>">
                                        </td>
                                    </tr>
                                </table>
                                <br/>
                            </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <?php if ($this->session->userdata('c_id')) { ?>
                                <div class="col-md-6 text-left">
                                    <a href="<?php echo base_url(); ?>admin/create_order" class="btn btn-info btn-lg">Add Product</a>
                                </div>
                            <?php } ?>
                            <?php if (!$this->session->userdata('c_id')) { ?>
                                <div class="col-md-6 text-left">
                                    <a href="<?php echo base_url(); ?>admin/order" class="btn btn-info btn-lg">Add Product</a>
                                </div>
                            <?php } ?>
                            <div class="col-md-6 text-right">
                                <button type="submit" class="btn btn-info btn-lg">Save Invoice</button>
                            </div>
                            <div></div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>