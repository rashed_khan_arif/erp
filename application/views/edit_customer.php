<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <a href="<?php echo base_url() ?>admin/add_customer" class="btn btn-info">Add Customer</a>
            <a href="<?php echo base_url() ?>admin/view_customer" class="btn btn-info">View Customer</a>
        </div>
        <?php
        $msg = $this->session->userdata('msg');
        if ($msg) {
            echo "<script>alert('$msg')</script>";
            $this->session->unset_userdata('msg');
        }
        ?>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-8 col-md-offset-2" style="border: solid 1px #ccc">
                    <form action="<?php echo base_url() ?>admin/update_customer" method="post">

                        <table class="table table-condensed table-hover table-striped table-bordered">
                            </tr>
                            <tr class="success msg">
                                <td colspan="2" class="text-center"><span style="font-size: 30px">Edit Customer</span></td>
                            </tr>
                            <tr class="">
                                <td><label for="fname" class="control-label">Customer ID</label></td>
                            <input type="hidden" class="box" value="<?php echo $select_customer->c_id ?>" name="c_id" id="fname">
                            <td><input type="text" class="box" value="<?php echo $select_customer->customer_id ?>" name="customer_id" required id="fname"></td>
                            </tr>
                            <tr class="">
                                <td><label for="lname" class="control-label">Organization Name</label></td>
                                <td><input type="text" class="box" value="<?php echo $select_customer->organization_name ?>" name="organization_name" required id="lname"></td>
                            </tr>
                            <tr class="">
                                <td><label for="bank" class="control-label">Reference</label></td>
                                <td><input type="text" class="box" value="<?php echo $select_customer->reference ?>" name="reference" id="bank"></td>
                            </tr>
                            <tr class="">
                                <td><label for="dob" class="control-label">Date_of_join</label></td>
                                <td><input type="date" class="box" value="<?php echo $select_customer->date_of_join ?>"  name="date_of_join" id="dob"></td>
                            </tr>
                            <tr class="">
                                <td><label for="id" class="control-label">Address</label></td>
                                <td><input type="text" class="box" value="<?php echo $select_customer->address ?>" name="address" id="id"></td>
                            </tr>
                            <tr class="">
                                <td><label for="ross" class="control-label">Phone</label></td>
                                <td><input type="text" class="box" value="<?php echo $select_customer->phone ?>" name="phone" id="ross"></td>
                            </tr>
                            <tr class="">
                                <td><label for="em" class="control-label">Email</label></td>
                                <td><input type="text" class="box" value="<?php echo $select_customer->email ?>" name="email" id="em"></td>
                            </tr>
                            <tr class="">
                                <td><label for="email" class="control-label">Credit</label></td>
                                <td>
                                    <input type="radio" onclick="off()" <?php
                                    if ($select_customer->credit == 'Limited') {
                                        echo "checked";
                                    }
                                    ?> name="credit" value="Limited">&nbsp;&nbsp;Limited
                                    <input type="radio" onclick="on()" name="credit" <?php
                                    if ($select_customer->credit == 'Un Limited') {
                                        echo "checked";
                                    }
                                    ?> value="Un Limited">&nbsp;&nbsp;Unlimited
                                </td>
                            </tr>
                            <script type="text/javascript">
                                function off() {
                                    document.getElementById("hidethis").style.display = '';
                                }
                                function on() {
                                    document.getElementById("hidethis").style.display = '';
                                }
                            </script>
                            <tr class="" id="hidethis" style="display: none">
                                <td><label class="control-label">Credit Amount</label></td>
                                <td><input type="number" class="box" value="<?php echo $select_customer->credit_amount ?>" name="credit_amount" ></td>
                            </tr>         
                            <tr class="">
                                <td><label class="control-label">Shipment Address</label></td>
                                <td>
                                    <textarea name="shipment_address" class="box"><?php echo $select_customer->shipment_address ?></textarea>
                                </td>
                            </tr>
                            <tr class="">
                                <td><label class="control-label"></label></td>
                                <td><input type="checkbox" required name="agree">&nbsp; &nbsp;All Information is correct</td>
                            </tr>
                            <tr class="">
                                <td colspan="2">
                                    <button type="submit" class="btn btn-info btn-block">Update Customer Information</button>
                                    <button type="reset" class="btn btn-block btn-default">Cancel</button>
                                </td>
                            </tr>
                        </table>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
