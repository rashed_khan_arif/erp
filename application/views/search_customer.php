<div class="container">
    <div class="panel panel-default">
        <div class="panel panel-heading">
            <a href="<?php echo base_url() ?>admin/add_customer" class="btn btn-info pull-left">Add New Customer</a>
            <a href="<?php echo base_url() ?>admin/view_customer" class="btn btn-info pull-left">View Customer</a>   
            <form class="form-inline" action="<?php echo base_url() ?>admin/search_customer" method="get">
                  <input type="submit" class="btn btn-info pull-right" required value="search">
                <input type="text" class="form-control pull-right" id="exampleInputAmount" required name="search" placeholder="type ID or name"> 
            </form><br/><br/>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-responsive table-bordered table-hover table-striped text-center tbl_color">
                        <thead>
                            <tr class="success">
                                <th class="text-center">Customer ID</th>
                                <th class="text-center">Organization Name</th>
                                <th class="text-center">Reference</th>
                                <th class="text-center">Date OF Join</th>
                                <th class="text-center">Phone</th> 
                                <th class="text-center">credit</th>
                                <th class="text-center">Credit Amount</th>       
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $msg = $this->session->userdata('msg');
                            if ($msg) {
                                ?>
                                <tr class="info msg" id="msg">
                                    <td colspan="12" class="text-center"><?php
                                        echo $msg;
                                        $this->session->unset_userdata('msg');
                                        ?></td>
                                </tr>
                            <?php } ?>
                            <?php foreach ($view_em as $v_em) { ?>
                                <tr>
                                    <td><?php echo $v_em->customer_id ?></td>
                                    <td><?php echo $v_em->organization_name ?></td>
                                    <td><?php echo $v_em->reference ?></td>
                                    <td><?php echo $v_em->date_of_join ?></td>

                                    <td><?php echo $v_em->phone ?></td>        
                                    <td><?php echo $v_em->credit ?></td>
                                    <td><?php echo $v_em->credit_amount ?></td>   
                                    <td>
                                        <script type="text/javascript">
                                            function check() {
                                                var chk = confirm('Are You sure ??');
                                                if (chk) {
                                                    return true;
                                                }
                                                eles{
                                                    return false;
                                                }
                                            }
                                        </script>
                                        <a class="btn btn-info btn-sm" title="Click To View" href="<?php echo base_url() ?>admin/view_customers/<?php echo $v_em->c_id ?>"> <li class="glyphicon glyphicon-eye-open">View</li></a>
                                        <a class="btn btn-success btn-sm" title="Click To Edit" href="<?php echo base_url() ?>admin/edit_customer/<?php echo $v_em->c_id ?>"> <li class="glyphicon glyphicon-edit">Edit</li></a>
                                        <a class="btn btn-danger btn-sm" title="Click To Delete" href="<?php echo base_url() ?>admin/delete_customer/<?php echo $v_em->c_id ?>"> <li class="glyphicon glyphicon-trash">Del</li></a>

                                    </td>
                                </tr>
                            <?php } ?>  

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>